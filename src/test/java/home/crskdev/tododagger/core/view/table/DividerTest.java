package home.crskdev.tododagger.core.view.table;

import home.crskdev.tododagger.core.view.ANSI;
import home.crskdev.tododagger.core.view.View;
import org.junit.Before;
import org.junit.Test;

import java.util.Collections;

import static home.crskdev.tododagger.core.view.table.Divider.SPACE;
import static home.crskdev.tododagger.core.view.table.VerticalDivider.Place;
import static org.junit.Assert.assertEquals;

/**
 * Created by criskey on 31/3/2017.
 */
public class DividerTest {
    private int w = 100;
    private int padding = 10;

    //TDD divider calculations
    private ANSI ansi;

    @Before
    public void setUp() throws Exception {
        ansi = new ANSI();
        ansi.enable = false;

    }

    @Test
    public void shouldHDividerHaveRightWidthFor1Col() {
        int cols = 1;
        HorizontalDivider divider = new HorizontalDivider(w, cols, padding);
        assertEquals(100 + 1 * 2 * padding + cols + 1, divider.width);
    }

    @Test
    public void shouldHDividerHaveRightWidthFor2Cols() {
        int cols = 2;
        HorizontalDivider divider = new HorizontalDivider(w, cols, padding);
        assertEquals(100 + 40 + cols + 1, divider.width);
    }

    @Test
    public void shouldHDividerHaveRightWidthFor3Cols() {
        int cols = 3;
        HorizontalDivider divider = new HorizontalDivider(w, cols, padding);
        assertEquals(100 + 60 + cols + 1, divider.width);
    }

    @Test
    public void shouldHDividerHaveRightWidthFor4Cols() {
        int cols = 4;
        HorizontalDivider divider = new HorizontalDivider(w, cols, padding);
        assertEquals(100 + 80 + cols + 1, divider.width);
    }

    @Test
    public void shouldDisplayHorizontal() {
        Divider divider = new HorizontalDivider(10, 3, 1, '$');
        assertEquals(String.join("", Collections.nCopies(10 + 3 * 1 * 2 + 3 + 1, "$")),
                ansi.inspect(divider.generate()));
    }

    @Test
    public void shouldShowCorrectVDivider() {
        VerticalDivider divider = new VerticalDivider(1);
        assertEquals("|" + SPACE, ansi.inspect(divider.generate()));
        divider.setPlace(Place.MIDDLE);
        assertEquals(SPACE + "|" + SPACE, ansi.inspect(divider.generate()));
        divider.setPlace(Place.END);
        assertEquals(SPACE + "|", ansi.inspect(divider.generate()));
    }
}