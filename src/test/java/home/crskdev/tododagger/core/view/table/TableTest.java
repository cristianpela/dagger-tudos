package home.crskdev.tododagger.core.view.table;

import home.crskdev.tododagger.core.view.ANSI;
import home.crskdev.tododagger.core.view.SystemView;
import home.crskdev.tododagger.core.view.View;
import home.crskdev.tododagger.core.view.table.schema.BooleanSchema;
import home.crskdev.tododagger.core.view.table.schema.DateSchema;
import home.crskdev.tododagger.core.view.table.schema.FloatSchema;
import home.crskdev.tododagger.core.view.table.schema.StringSchema;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Collection;
import java.util.Date;
import java.util.Iterator;

import static org.junit.Assert.assertEquals;


/**
 * Created by criskey on 24/3/2017.
 */
@RunWith(MockitoJUnitRunner.class)
public class TableTest {

    View view = new SystemView(new ANSI());

    @Test
    public void shouldPassWhenAreFixedColumns() {
        Table table = new Table(view, 3);
        table.addRow("a", "b", "c");
        Collection<Row> rows = table.getRows();
        Row row = rows.iterator().next();
        assertEquals(3, row.getCells().size());

        Iterator<Cell> iterator = row.getCells().iterator();
        Cell col1 = iterator.next();
        assertEquals(Table.DEFAULT_WIDTH, col1.schema.width);
        assertEquals("a", col1.content);

        Cell col2 = iterator.next();
        assertEquals(Table.DEFAULT_WIDTH, col2.schema.width);
        assertEquals("b", col2.content);

        Cell col3 = iterator.next();
        assertEquals(Table.DEFAULT_WIDTH, col3.schema.width);
        assertEquals("c", col3.content);
    }

    @Test(expected = IllegalStateException.class)
    public void shouldFailWhenAreContentsLenDontMatchSchemasLen() {
        Table table = new Table(view, 30, 40, 50);
        table.addRow("a", null);
    }

    @Test(expected = IllegalStateException.class)
    public void shouldFailWhenContentTypeDoesntMatchSchemaType() {
        Table table = new Table(view, new StringSchema(10));
        table.addRow(3);
    }

    @Test
    public void shouldDisplay() {
        Table table = new Table(view, 5, 5, 5);
        table.addHeaderRow("av", "b", "cd");
        table.addRow("av", "aaaaabbbbb", "caaaaabbbbb");
        table.addRow("avssss222", "2333dsdas", "caaaaabbbbb");
        table.display();
    }

    @Test
    public void shouldDisplayBySchema() {

        DividersSpec pattern = new DividersSpec('-', ANSI.Color.MAGENTA, '|', ANSI.Color.GREEN);

        Table table = new Table(view,
                pattern,
                new StringSchema(7),
                new BooleanSchema(10),
                new StringSchema(10),
                new FloatSchema(10, 2),
                new DateSchema(15));

        table.addCellRenderer((row, col, cell) -> {
            if (col == 0 && row > 0) {
                return new RenderedCell(cell.schema.width,
                        Alignment.Type.CENTER,
                        cell.content.toString());
            }
            return null;
        });

        table.addCellDecorator((row, col, decorator, cell) -> {
            if (!cell.isHeader && row % 2 != 0) { //odd row make them green
                return decorator.background(ANSI.Color.GREEN)
                        .foreground(ANSI.Color.CYAN)
                        .create();
            }
            return null;
        });

        table.addHeaderRow("Title1", "Title2", "Title3", "Title4", "Title5");
        table.addRow("1)", true, "caaaaabbbbbfsdfdsffdsfsdf", 34.56323f, new Date(System.currentTimeMillis()));
        table.addRow("2)", false, "caaaaabbbbbfsdfdsffdsfsdf", 34.56f, new Date(System.currentTimeMillis()));
        table.addRow("3)", true, "caaaaabbbbbfsdfdsffdsfsdf", 34.56f, new Date(System.currentTimeMillis()));
        table.display();
    }

    @Test
    public void shouldDisplayEmptyBySchema() {
        Table table = new Table(view,
                new StringSchema(7),
                new BooleanSchema(10),
                new StringSchema(10),
                new FloatSchema(10, 2),
                new DateSchema(15));
        table.display();
    }

}