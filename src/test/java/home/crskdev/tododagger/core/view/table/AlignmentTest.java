package home.crskdev.tododagger.core.view.table;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by criskey on 4/4/2017.
 */
public class AlignmentTest {

    String text = "abc";
    int width = 10;

    @Test
    public void shouldAlignLeft() {
        String align = new Alignment(Alignment.Type.LEFT, width).align(text);
        assertEquals(text + spaces(width - text.length()), align);
        assertEquals(width, align.length());

    }

    @Test
    public void shouldAlignRight() {
        String align = new Alignment(Alignment.Type.RIGHT, width).align(text);
        assertEquals(spaces(width - text.length()) + text, align);
        assertEquals(width, align.length());
    }

    @Test
    public void shouldAlignCenter() {
        String align = new Alignment(Alignment.Type.CENTER, width).align(text);
        assertEquals(spaces(4) + text + spaces(3), align);
        assertEquals(width, align.length());
    }

    private String spaces(int width) {
        return String.format("%" + width + "s", "");
    }

}