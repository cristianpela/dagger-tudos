package home.crskdev.tododagger.core.view.table;

import home.crskdev.tododagger.core.view.Decorator;
import org.junit.Ignore;
import org.junit.Test;

import static home.crskdev.tododagger.core.view.ANSI.Color.MAGENTA;
import static home.crskdev.tododagger.core.view.ANSI.Color.YELLOW;

/**
 * Created by criskey on 6/4/2017.
 */
public class DecoratorTest {

    @Test
    @Ignore
    public void get() {
        String text = "ABC";
        String decorated = Decorator.builder(text)
                .background(MAGENTA)
                .foreground(YELLOW)
                .bold()
                .blink()
                .underlined()
                .create();
        System.out.println(decorated);
    }

}