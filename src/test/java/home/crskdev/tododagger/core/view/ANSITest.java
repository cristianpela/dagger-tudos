package home.crskdev.tododagger.core.view;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by criskey on 7/4/2017.
 */
public class ANSITest {
    @Test
    public void shouldBeSameTextIfANSIisDisabled() {

        ANSI ansi = new ANSI();
        String text = "abc";
        String decoText = Decorator.builder(text)
                .background(ANSI.Color.BLUE)
                .bold()
                .underlined()
                .create();
        assertEquals(text+text, ansi.inspect(decoText+decoText));
        System.out.println(decoText.length());
    }

}