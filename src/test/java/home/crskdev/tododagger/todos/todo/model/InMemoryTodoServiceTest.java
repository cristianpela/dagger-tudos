package home.crskdev.tododagger.todos.todo.model;

import home.crskdev.tododagger.todos.todo.BaseTest;
import org.junit.Test;

import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by criskey on 17/3/2017.
 */
public class InMemoryTodoServiceTest extends BaseTest {

    List<Todo> todos;

    @Override
    public void setup() {
        todos = new ArrayList<>(Arrays.asList(
                new Todo(1, "todo1"),
                new Todo(2, "todo2"),
                new Todo(3, "todo3")));
        Map<String, List<Todo>> usersTodo = new HashMap<>();
        usersTodo.put("foo", todos);
        service = new InMemoryTodoService("foo", usersTodo);
    }

    @Test
    public void findAll()  {
        assertEquals(todos, service.findAll());
    }

    @Test
    public void find()  {
        //oneFound found
        Collection<Todo> oneFound = service.find(1);
        assertTrue(!oneFound.isEmpty());
        assertEquals(todos.get(0), oneFound.iterator().next());

        //no found
        Collection<Todo> notFound = service.find(203, 43, 20);
        assertTrue(notFound.isEmpty());

        //all found
        Collection<Todo> allFound = service.find(1, 2);
        assertTrue(!allFound.isEmpty());
        Iterator<Todo> it = allFound.iterator();
        assertEquals(todos.get(0).id, it.next().id);
        assertEquals(todos.get(1).id, it.next().id);

        //partial found
        Collection<Todo> partialFound = service.find(1, 499, 2, 6000);
        assertTrue(allFound.size() == 2);
        it = partialFound.iterator();
        assertEquals(todos.get(0).id, it.next().id);
        assertEquals(todos.get(1).id, it.next().id);
    }

    @Test
    public void findAllIds()  {
        assertEquals(Arrays.asList(new Integer[]{1, 2, 3}), service.findAllIds());
    }

    @Test
    public void add()  {
        service.add("new task home.crskdev.tododagger.core.todo");
        assertEquals(todos.size(), 4);
        assertEquals("new task home.crskdev.tododagger.core.todo", todos.get(todos.size() - 1).task);
    }

    @Test
    public void shouldRemoveOne()  {
        service.remove(1);
        Todo deleted = null;
        for (Todo t : todos) {
            if (t.id == 1) {
                deleted = t;
                break;
            }
        }
        assertTrue("Todo should be removed", deleted == null);
    }

    @Test
    public void shouldRemoveBatch()  {
        service.remove(2, 3);
        assertEquals(todos.size(), 1);
        assertEquals(1, todos.get(0).id);
    }

    @Test
    public void shouldRemovePartialBatchExceptOne()  {
        // we managed to delete all ids but 4, becaus there is no _todo with id 4
        thrown.expectMessage("Todos removed except ids: 4");
        service.remove(2, 3, 4);
    }


    @Test
    public void shouldRemoveAll()  {
        service.removeAll();
        assertEquals(0, todos.size());
    }

    @Test
    public void shouldCompleteExcept2()  {
        thrown.expectMessage("Todos were marked as complete except ids: 4, 5");
        service.complete(true, 1, 2, 3, 4, 5);
    }

    @Test
    public void shouldCompeteAll()  {
        service.completeAll(true);
        for (Todo todo : todos) {
            assertTrue("Todo must be completed ", todo.completed);
        }
    }

    @Test
    public void shouldFailEdit()  {
        thrown.expectMessage("Todo with id 100 not found");
        service.edit(100, "bogus task");
    }

}