package home.crskdev.tododagger.todos.todo;

import home.crskdev.tododagger.core.command.CommandException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.fail;
import static org.mockito.Mockito.mock;

/**
 * Created by criskey on 17/3/2017.
 */
@RunWith(MockitoJUnitRunner.class)
public class AddCommandTest extends BaseTest {

    @Before
    public void setup() {
        command = new AddCommand(view, service);
    }

    @Test
    public void testShouldFailIfTodoIsEmpty() {
        thrown.expectMessage("The home.crskdev.tododagger.core.todo is empty");
        command.execute(command.name());
    }

    @Test
    public void testShouldAddNewTodo() {
        try {
            command.execute(command.name(), "foo");
        } catch (CommandException ex) {
            fail();
        }
    }

}