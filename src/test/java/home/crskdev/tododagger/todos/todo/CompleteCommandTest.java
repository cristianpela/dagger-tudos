package home.crskdev.tododagger.todos.todo;

import home.crskdev.tododagger.core.command.CommandException;
import home.crskdev.tododagger.todos.todo.model.Todo;
import home.crskdev.tododagger.todos.todo.model.TodoException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.fail;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.Mockito.doThrow;

/**
 * Created by criskey on 17/3/2017.
 */
@RunWith(MockitoJUnitRunner.class)
public class CompleteCommandTest extends BaseTest {

    @Override
    public void setup() {
        super.setup();
        command = new CompleteCommand(view, service);
    }

    @Test
    public void testShouldFailWhenArgsAreMoreThan3() {
        thrown.expectMessage("Must provide two or three arguments. " +
                "\nExample: complete <id> <'yes'|'no'> or complete <id>");
        command.execute(command.name(), "2", "yes", "no", "yes");
    }

    @Test
    public void testShouldFailWhenArgsAreLessThan2() {
        thrown.expectMessage("Must provide two or three arguments. " +
                "\nExample: complete <id> <'yes'|'no'> or complete <id>");
        command.execute(command.name());
    }

    @Test
    public void testShouldFailIfTodoDoesNotExist() throws Exception {
        doThrow(TodoException.class).when(service).complete(anyBoolean(), any());
        thrown.expect(CommandException.class);
        command.execute(command.name(), "" + 100);
    }

    @Test
    public void testShouldMarkAsComplete1() {
        Todo todo = randomTodo();
        try {
            command.execute(command.name(), "" + todo.id);
        } catch (CommandException ex) {
            fail();
        }
    }

    @Test
    public void testShouldMarkAsComplete2() {
        Todo todo = randomTodo();
        try {
            command.execute(command.name(), "" + todo.id, "yes");
        } catch (CommandException ex) {
            fail();
        }
    }

    @Test
    public void testShouldMarBatchkAsComplete() {
        try {
            command.execute(command.name(), "1-2-3-4");
        } catch (CommandException ex) {
            fail();
        }
    }

    @Test
    public void testShouldMarBatchkAsUnComplete() {
        try {
            command.execute(command.name(), "1-2-3-4", "no");
        } catch (CommandException ex) {
            fail();
        }
    }

    @Test
    public void testShouldMarkAsUnComplete() {
        Todo todo = randomTodo();
        try {
            command.execute(command.name(), "" + todo.id, "no");
        } catch (CommandException ex) {
            fail();
        }
    }

    @Test
    public void testShouldMarkPartialBatchComplete() {
        doThrow(TodoException.class).when(service).complete(true, 1, 2, 3, 4);
        thrown.expect(CommandException.class);
        command.execute(command.name(), "1-2-3-4");
    }

    @Test
    public void testShouldFail2ndArgIsNotYesOrNo() {
        Todo todo = randomTodo();
        thrown.expectMessage("The second argument must be 'yes' or 'no'");
        command.execute(command.name(), "" + todo.id, "no-yes-or-no");
    }

}