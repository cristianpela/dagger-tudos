package home.crskdev.tododagger.todos.todo.model;

import com.rethinkdb.net.Connection;
import home.crskdev.tododagger.core.LazyCache;
import org.junit.*;
import org.junit.runners.MethodSorters;

import java.util.Collection;
import java.util.UUID;

import static com.rethinkdb.RethinkDB.r;

/**
 * Created by criskey on 1/4/2017.
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class RethinkInMemoryTodoServiceTest {


    RethinkTodoService service;

    LazyCache<Connection> lazyCon;

    @Before
    public void setUp() throws Exception {
        lazyCon = new LazyCache<Connection>() {
            @Override
            protected Connection init() {
                return r.connection()
                        .hostname("localhost")
                        .port(28015)
                        .connect();
            }
        };
        service = new RethinkTodoService(
                "cristi",
                lazyCon
        );
    }

    @After
    public void tearDown() throws Exception {
        lazyCon.get().close();
    }

    @Test
    @Ignore
    public void a_testAddTodo() {
        String task = "Test task: " + UUID.randomUUID().toString();
        service.add(task);
    }

    @Test
    @Ignore
    public void b_testFindAll() {
        System.out.println(service.findAll());
    }

    @Test
    @Ignore
    public void c_testDeletAll() {
        service.removeAll();
    }

    @Test
    @Ignore
    public void c_testDeleteSome() {
        service.remove(1, 2);
    }

    @Test
    @Ignore
    public void d_testCompleteSome() {
        service.complete(true, 1, 100);
    }

    @Test
    @Ignore
    public void e_testCompleteAll() {
        service.completeAll(false);
    }

    @Test
    @Ignore
    public void f_testEdit() {
        service.edit(1, "Edited todo task");
    }

    @Test
    @Ignore
    public void g_testfindSome() {
        Collection<Todo> todos = service.find(1, 2);
        System.out.println(todos);
    }
}