package home.crskdev.tododagger.todos.todo;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.Mockito.*;

/**
 * Created by criskey on 17/3/2017.
 */
@RunWith(MockitoJUnitRunner.class)
public class BaseTodoCommandTest extends BaseTest {

    @Test
    public void shouldFailWhenBadCommand() {
        BaseTodoCommand command = mock(BaseTodoCommand.class, CALLS_REAL_METHODS);
        when(command.name()).thenReturn("foo");
        thrown.expectMessage("Command failfoo unkwnon");
        command.execute("failfoo");
    }

}