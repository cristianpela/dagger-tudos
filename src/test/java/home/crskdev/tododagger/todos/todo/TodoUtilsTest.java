package home.crskdev.tododagger.todos.todo;

import home.crskdev.tododagger.core.command.CommandException;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by criskey on 17/3/2017.
 */
public class TodoUtilsTest extends BaseTest {

    @Override
    public void setup() {
        super.setup();
    }

    @Test
    public void testShouldFailWhenTodoIdIsNotANumber() {
        idFormatFailure("not-a-number");
    }

    @Test
    public void testShouldFailWhenTodoIdIsANegativeNumber() {
        idFormatFailure("-1");
    }

    @Test
    public void testShouldFailWhenTodoIdIsAFloatingNumber() {
        idFormatFailure("2.3");
    }

    @Test
    public void testShouldReturnAnArrayWhenBatchPasses() {
        Assert.assertArrayEquals(new Integer[]{1, 2, 3},  TodoUtils.checkBatchIdValidity(" 1-2 -3"));
        Assert.assertArrayEquals(new Integer[]{1, 2, 3},  TodoUtils.checkBatchIdValidity("1-2 -3-"));
        Assert.assertArrayEquals(new Integer[]{1, 2, 3},  TodoUtils.checkBatchIdValidity("1 -2-3-"));
    }

    @Test
    public void testShouldFailWhenBatchBadFormated() {
        thrown.expectMessage("Todo id must be a positive integer number");
        TodoUtils.checkBatchIdValidity("1-2* -3");
    }

    private void idFormatFailure(String strId) {
        thrown.expectMessage("Todo id must be a positive integer number");
        TodoUtils.checkIdValidity(strId);
    }
}