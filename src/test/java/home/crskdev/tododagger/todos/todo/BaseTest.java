package home.crskdev.tododagger.todos.todo;

import home.crskdev.tododagger.core.command.Command;
import home.crskdev.tododagger.core.view.View;
import org.junit.Before;
import org.junit.Rule;
import org.junit.rules.ExpectedException;
import home.crskdev.tododagger.todos.todo.model.ITodoService;
import home.crskdev.tododagger.todos.todo.model.Todo;
import org.mockito.Mock;

import java.util.UUID;

import static org.mockito.Mockito.mock;

/**
 * Created by criskey on 17/3/2017.
 */

public class BaseTest {

    public Command command;

    @Mock
    public ITodoService service;

    @Mock
    public View view;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Before
    public void setup() {
        service = mock(ITodoService.class);
    }

    protected Todo randomTodo() {
        return new Todo(UUID.randomUUID().toString());
    }

}
