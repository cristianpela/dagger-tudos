package home.crskdev.tododagger.todos.todo;

import home.crskdev.tododagger.core.command.CommandException;
import home.crskdev.tododagger.todos.todo.model.TodoException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.Collections;

import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;

/**
 * Created by criskey on 17/3/2017.
 */
@RunWith(MockitoJUnitRunner.class)
public class RemoveCommandTest extends BaseTest {

    @Override
    public void setup() {
        super.setup();
        command = new RemoveCommand(view, service);
    }

    @Test
    public void testShouldPassPartialRemoveBatch() {
        doThrow(TodoException.class).when(service).remove(1,2,3);
        thrown.expect(CommandException.class);
        command.execute(command.name(), "1-2-3");
    }

    @Test
    public void testShouldFailWhenRemoveAllBadFormat() {
        thrown.expect(CommandException.class);
        command.execute(command.name(), "All", "some random");
    }
}