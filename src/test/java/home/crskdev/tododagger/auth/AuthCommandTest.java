package home.crskdev.tododagger.auth;

import home.crskdev.tododagger.auth.model.User;
import home.crskdev.tododagger.core.command.CommandException;
import home.crskdev.tododagger.hub.di.HubComponent;
import home.crskdev.tododagger.todos.TodosComponent;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import javax.inject.Provider;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

/**
 * Created by criskey on 19/3/2017.
 */
@RunWith(MockitoJUnitRunner.class)
public class AuthCommandTest extends BaseAuthTest {

    @Mock
    Provider<HubComponent.Builder> provider;

    @Mock
    HubComponent.Builder builder;

    @Before
    public void setUp() throws Exception {
        command = new AuthCommand(provider, service);
    }

    @Test
    public void testSuccessLogin() {
        when(provider.get()).thenReturn(builder);
        when(builder.build()).thenReturn(mock(HubComponent.class));
        command.execute(command.name(), "foo", "123");
        verify(provider.get()).build();
    }

    @Test
    public void testShouldFailWhenArgumentsAreInvalid(){
        thrown.expect(CommandException.class);
        command.execute(command.name(), "bad");
    }

}