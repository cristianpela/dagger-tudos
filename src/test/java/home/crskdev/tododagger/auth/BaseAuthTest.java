package home.crskdev.tododagger.auth;

import home.crskdev.tododagger.auth.model.IAuthService;
import home.crskdev.tododagger.core.command.Command;
import home.crskdev.tododagger.core.view.View;
import org.junit.Before;
import org.junit.Rule;
import org.junit.rules.ExpectedException;
import org.mockito.Mock;

/**
 * Created by criskey on 20/3/2017.
 */
public class BaseAuthTest {

    @Mock
    protected IAuthService service;

    @Mock
    public View view;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    protected Command command;

    @Before
    public void setUp() throws Exception {
    }

}
