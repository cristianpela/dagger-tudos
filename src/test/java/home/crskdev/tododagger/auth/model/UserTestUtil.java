package home.crskdev.tododagger.auth.model;

import home.crskdev.tododagger.account.details.UserDetails;

import java.util.UUID;

/**
 * Created by criskey on 20/3/2017.
 */
public final class UserTestUtil {

    private UserTestUtil() {
    }

    public static User getFooUser() {
        return new User("foo", "bar".toCharArray());
    }

    public static User randomUser() {
        return new User(randomString(), randomString().toCharArray());
    }

    public static UserDetails randomUserDetails(String username) {
        return new UserDetails.Builder(username)
                .setFirstName(randomString())
                .setLastName(randomString())
                .setBirthdate(System.currentTimeMillis())
                .setAbout(randomString())
                .create();
    }

    private static String randomString() {
        return UUID.randomUUID().toString();
    }

}