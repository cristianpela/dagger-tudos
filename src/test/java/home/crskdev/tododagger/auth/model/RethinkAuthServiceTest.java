package home.crskdev.tododagger.auth.model;

import com.rethinkdb.net.Connection;
import home.crskdev.tododagger.account.details.UserDetails;
import home.crskdev.tododagger.auth.BCryptPasswordEncoder;
import home.crskdev.tododagger.core.LazyCache;
import org.junit.*;
import org.junit.runners.MethodSorters;

import static com.rethinkdb.RethinkDB.r;

/**
 * Created by criskey on 31/3/2017.
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class RethinkAuthServiceTest {

    LazyCache<Connection> conn;

    RethinkAuthService authService;

    @Before
    @Ignore
    public void setUp() throws Exception {
        conn = new LazyCache<Connection>() {
            @Override
            protected Connection init() {
                return r.connection().hostname("localhost").connect();
            }
        };
        authService = new RethinkAuthService(conn, new BCryptPasswordEncoder());
    }

    @After
    @Ignore
    public void tearDown() throws Exception {
        conn.get().close();
    }

    @Test
    @Ignore
    public void b_login() {
        authService.login(UserTestUtil.getFooUser());
    }

    @Test(expected = AuthException.class)
    @Ignore
    public void a_register() {
        authService.register(UserTestUtil.getFooUser());
    }

    @Test
    @Ignore
    public void c_changePassword() {
        authService.changePassword(UserTestUtil.getFooUser(), "newpass".toCharArray());
    }

    @Test
    @Ignore
    public void setUserDetails() {
        authService.setUserDetails(UserTestUtil.randomUserDetails(
                UserTestUtil.getFooUser().name
        ));
    }

    @Test
    @Ignore
    public void getUserDetails() {
        UserDetails userDetails = authService.getUserDetails("foo");
        System.out.println(userDetails);
    }


}