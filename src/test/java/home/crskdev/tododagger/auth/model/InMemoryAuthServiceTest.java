package home.crskdev.tododagger.auth.model;

import home.crskdev.tododagger.account.details.UserDetails;
import home.crskdev.tododagger.auth.BaseAuthTest;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.Set;
import java.util.TreeSet;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

/**
 * Created by criskey on 19/3/2017.
 */
@RunWith(MockitoJUnitRunner.class)
public class InMemoryAuthServiceTest extends BaseAuthTest {

    IAuthService authService;

    @Before
    public void setUp() throws Exception {
        authService = new AuthServiceProxy(new InMemoryAuthService(users()));
    }

    @Test
    public void succedLogin() throws Exception {
        fooLogin();
        User principal = authService.getPrincipal();
        assertEquals(UserTestUtil.getFooUser().name, principal.name);

        char[] emptyPass = new char[principal.password.length];
        Arrays.fill(emptyPass, ' ');
        assertArrayEquals(emptyPass, principal.password);
    }

    @Test(expected = AuthException.class)
    public void succedLoginFailedBadUser() throws Exception {
        authService = new InMemoryAuthService(users());
        authService.login(new User("foos", "123".toCharArray()));
    }

    @Test(expected = AuthException.class)
    public void succedLoginFailedBadPassword() throws Exception {
        authService = new InMemoryAuthService(users());
        authService.login(new User("foo", "124".toCharArray()));
    }

    @Test(expected = AuthException.class)
    public void succedLoginFailedBadUserAndPassword() throws Exception {
        authService = new InMemoryAuthService(users());
        authService.login(new User("foosss", "124".toCharArray()));
    }


    @Test
    public void succedRegister() {
        authService.register(new User("auser", "bar".toCharArray()));
    }

    @Test(expected = AuthException.class)
    public void failedRegisterDuplicateUser() {
        authService.register(new User("foo", "12365".toCharArray()));
    }

    @Test(expected = AuthException.class)
    public void failedRegisterDuplicateUserAndPassword() {
        authService.register(UserTestUtil.getFooUser());
    }

    @Test
    public void shouldChangePassword() {
        authService.login(UserTestUtil.getFooUser());

        authService.changePassword(UserTestUtil.getFooUser(), "newpass".toCharArray());
        //change again
        authService.changePassword(new User("foo", "newpass".toCharArray()), "newpass2".toCharArray());
    }

    @Test(expected = AuthException.class)
    public void shouldFailToChangeIfTheOldPasswordIsWrong() {
        authService.login(UserTestUtil.getFooUser());

        char[] newpass = "newpass".toCharArray();
        authService.changePassword(new User("foo", "badoldpass".toCharArray()), newpass);
    }

    @Test(expected = AuthException.class)
    public void shouldFailChangePasswordByOtherThanPrincipal() {
        Set<User> users = users();
        User other = UserTestUtil.randomUser();
        users.add(other);
        authService = new AuthServiceProxy(new InMemoryAuthService(users));
        authService.login(other);
        char[] newpass = "newpass".toCharArray();
        authService.changePassword(UserTestUtil.getFooUser(), newpass);
    }

    @Test(expected = AuthException.class)
    public void shouldFailChangePasswordIfNotLoggedIn() {
        char[] newpass = "newpass".toCharArray();
        authService.changePassword(UserTestUtil.getFooUser(), newpass);
    }


    @Test(expected = AuthException.class)
    public void shouldFailRegisterWhenLoggedIn() {
        authService.login(UserTestUtil.getFooUser());
        authService.register(UserTestUtil.randomUser());
    }

    @Test(expected = AuthException.class)
    public void shouldFailToGetUserDetailsIfNotLoggedIn() {
        authService = new AuthServiceProxy(new InMemoryAuthService());
        authService.getUserDetails("foo");
    }

    @Test(expected = AuthException.class)
    public void shouldFailToSetUserDetailsIfNotLoggedIn() {
        authService = new AuthServiceProxy(new InMemoryAuthService());
        authService.getUserDetails("foo");
    }

    @Test
    public void shouldFailToSetUserDetailsIfNotPrincipal() {
        thrown.expectMessage("You can not change other user's details");
        fooLogin();
        authService.setUserDetails(UserTestUtil.randomUserDetails("some-user"));
    }

    @Test
    public void shouldSuccessSetUserDetails() {
        fooLogin();
        UserDetails details = UserTestUtil.randomUserDetails("foo");
        authService.setUserDetails(details);
        assertEquals(details, authService.getUserDetails("foo"));
    }

    private void fooLogin() {
        authService = new AuthServiceProxy(new InMemoryAuthService(users()));
        authService.login(UserTestUtil.getFooUser());
    }

    private Set<User> users() {
        User u = UserTestUtil.getFooUser();
        Set<User> users = new TreeSet();
        users.add(u);
        return users;
    }

}