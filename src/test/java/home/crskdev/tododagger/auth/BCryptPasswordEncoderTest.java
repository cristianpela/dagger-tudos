package home.crskdev.tododagger.auth;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by criskey on 31/3/2017.
 */
public class BCryptPasswordEncoderTest {

    private IPasswordEncoder encoder;

    @Before
    public void setUp() throws Exception {
        encoder = new BCryptPasswordEncoder();

    }

    @Test
    public void matches() {
        String pass = "mypassword";
        String hash = encoder.encode(pass.toCharArray(), null);
        assertTrue(encoder.matches(pass.toCharArray(), hash, null));

        assertFalse(encoder.matches("badpassword".toCharArray(), hash, null));
    }

}