package home.crskdev.tododagger.auth;

import home.crskdev.tododagger.auth.model.AuthException;
import home.crskdev.tododagger.auth.model.User;
import home.crskdev.tododagger.core.command.CommandException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doThrow;

/**
 * Created by criskey on 20/3/2017.
 */
@RunWith(MockitoJUnitRunner.class)
public class RegisterCommandTest extends BaseAuthTest {

    @Before
    public void setUp() throws Exception {
        command = new RegisterCommand(view, service);
    }

    @Test
    public void shouldFailRegister() {
        doThrow(AuthException.class).when(service).register(any(User.class));
        thrown.expect(CommandException.class);
        command.execute(command.name(), "foo", "123");
    }


    @Test
    public void shouldFailWhenArgsAreWrong() {
        thrown.expect(CommandException.class);
        command.execute(command.name(), "foo", "123", "extrawrong");
    }

}