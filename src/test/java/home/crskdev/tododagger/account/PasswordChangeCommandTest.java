package home.crskdev.tododagger.account;

import home.crskdev.tododagger.auth.BaseAuthTest;
import home.crskdev.tododagger.auth.model.AuthException;
import home.crskdev.tododagger.auth.model.User;
import home.crskdev.tododagger.core.command.CommandException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.Mockito.any;
import static org.mockito.Mockito.doThrow;

/**
 * Created by criskey on 21/3/2017.
 */
@RunWith(MockitoJUnitRunner.class)
public class PasswordChangeCommandTest extends BaseAuthTest {

    @Override
    public void setUp() throws Exception {
        command = new PasswordChangeCommand(view, "foo", service);
    }

    @Test(expected = CommandException.class)
    public void shouldFailWhenArgsAreNot3() {
        command.execute(command.name(), "new_pass");
    }

    @Test(expected = CommandException.class)
    public void shouldFailWhenOldPassIsWrong() {
        doThrow(AuthException.class).when(service).changePassword(any(User.class), any(char[].class));
        command.execute(command.name(), "wrong_old_pass", "new_pass");
    }


    @Test
    public void shouldPassChangingThePassword() {
        command.execute(command.name(), "old_pass", "new_pass");
    }
}