package home.crskdev.tododagger.account.details;

import home.crskdev.tododagger.account.di.AccountComponent;
import home.crskdev.tododagger.auth.model.AuthException;
import home.crskdev.tododagger.auth.model.IAuthService;
import home.crskdev.tododagger.auth.model.UserTestUtil;
import home.crskdev.tododagger.core.command.CommandException;
import home.crskdev.tododagger.core.view.View;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import javax.inject.Provider;

import static home.crskdev.tododagger.account.details.EditDetailsCommand.DATE_FORMAT;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

/**
 * Created by criskey on 23/3/2017.
 */
@RunWith(MockitoJUnitRunner.class)
public class EditDetailsCommandTest {

    EditDetailsCommand command;

    @Mock
    View view;

    @Mock
    IAuthService service;

    @Mock
    Provider<AccountComponent.Builder> provider;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Before
    public void setUp() throws Exception {
        when(service.getUserDetails(anyString())).thenReturn(UserTestUtil.randomUserDetails("foo"));
        when(service.getPrincipal()).thenReturn(UserTestUtil.getFooUser());
        command = new EditDetailsCommand(view, service, provider);
    }

    @Test(expected = CommandException.class)
    public void shouldFailWhenEmpty() {
        command.step = 1;
        command.execute("");
    }

    @Test(expected = CommandException.class)
    public void shouldFailDateFormatIncorrect() {
        command.step = 3;
        command.execute("22-34-3435");
    }

    @Test
    public void shouldPassSavingUserDetails() {
        prepareProvider();
        command.execute();
        verify(view).println("Enter your first name:");
        command.execute("John");
        verify(view).println("Enter your last name:");
        command.execute("Doe");
        verify(view).println("Enter your birth date (" + DATE_FORMAT + "):");
        command.execute("22.04.1234");
        verify(view).println("Enter your details about you:");
        command.execute("about me");
        verify(view).println("Details saved.");
    }

    @Test
    public void shouldFailToSaveUserDetailsWhenServiceError() {
        prepareProvider();

        String serviceErr = "Service error";
        doThrow(new AuthException(serviceErr))
                .when(service)
                .setUserDetails(any(UserDetails.class));

        thrown.expectMessage("Failed to save user details. Reason: " + serviceErr +" .Try again");

        command.execute();
        verify(view).println("Enter your first name:");
        command.execute("John");
        verify(view).println("Enter your last name:");
        command.execute("Doe");
        verify(view).println("Enter your birth date (" + DATE_FORMAT + "):");
        command.execute("22.04.1234");
        verify(view).println("Enter your details about you:");
        command.execute("about me");
    }

    private void prepareProvider() {
        AccountComponent.Builder builder = mock(AccountComponent.Builder.class);
        when(provider.get()).thenReturn(builder);
        when(builder.build()).thenReturn(mock(AccountComponent.class));
    }
}