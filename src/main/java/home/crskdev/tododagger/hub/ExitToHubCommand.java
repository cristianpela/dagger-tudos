package home.crskdev.tododagger.hub;

import home.crskdev.tododagger.core.command.GoToSectionCommand;
import home.crskdev.tododagger.hub.di.HubComponent;

import javax.inject.Provider;

/**
 * Created by criskey on 21/3/2017.
 */
public class ExitToHubCommand extends GoToSectionCommand<HubComponent.Builder> {

    public static final String HUB_SECTION = "hub";

    public ExitToHubCommand(Provider<HubComponent.Builder> provider) {
        super(provider);
    }

    @Override
    public String name() {
        return HUB_SECTION;
    }
}
