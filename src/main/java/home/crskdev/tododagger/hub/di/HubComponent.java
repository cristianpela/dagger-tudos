package home.crskdev.tododagger.hub.di;

import dagger.Subcomponent;
import home.crskdev.tododagger.core.Init;
import home.crskdev.tododagger.core.InitComponent;

import javax.inject.Named;

/**
 * Created by criskey on 21/3/2017.
 */
@Subcomponent(modules = HubModule.class)
public interface HubComponent extends InitComponent {

    @Override
    @Named("hub-entry-point")
    Init init();

    @Subcomponent.Builder
    interface Builder extends InitComponent.Builder<HubComponent>{
        HubComponent build();
    }
}
