package home.crskdev.tododagger.hub.di;

import javax.inject.Scope;

/**
 * Created by criskey on 7/4/2017.
 */
@Scope
public @interface HubScope {
}
