package home.crskdev.tododagger.hub.di;

import dagger.Module;
import dagger.Provides;
import dagger.multibindings.IntoMap;
import dagger.multibindings.StringKey;
import home.crskdev.tododagger.account.di.AccountComponent;
import home.crskdev.tododagger.auth.LogoutCommand;
import home.crskdev.tododagger.auth.di.AuthComponent;
import home.crskdev.tododagger.auth.di.Principal;
import home.crskdev.tododagger.core.Init;
import home.crskdev.tododagger.core.command.*;
import home.crskdev.tododagger.core.view.View;
import home.crskdev.tododagger.settings.di.SettingsComponent;
import home.crskdev.tododagger.todos.TodosComponent;

import javax.inject.Named;
import javax.inject.Provider;
import java.io.Closeable;
import java.util.Map;

import static home.crskdev.tododagger.auth.LogoutCommand.LOGOUT_SECTION;
import static home.crskdev.tododagger.core.command.SystemExitCommand.EXIT;
import static home.crskdev.tododagger.core.command.WelcomeCommand.WELCOME;

/**
 * Created by criskey on 21/3/2017.
 */
@Module(subcomponents = {
        AccountComponent.class,
        TodosComponent.class,
        SettingsComponent.class
})
public class HubModule {

    public static final String TODOS_SECTION = "todos";

    public static final String ACCOUNT_SECTION = "account";

    public static final String SETTINGS_SECTION = "settings";

    @Provides
    @Named("hub-entry-point")
    public Init provideEntryPoint(View view, @HubCommands Map<String, Command> commands,
                                  ListenerSwapper setter) {
        return new CommandManager(view, commands, setter);
    }

    @Provides
    @IntoMap
    @HubCommands
    @StringKey(WELCOME)
    public Command provideWelcomeCommand(View view, @Principal String principal) {
        return new WelcomeCommand(view, principal) {
            @Override
            public String[] availableCommands() {
                return new String[]{
                        TODOS_SECTION,
                        ACCOUNT_SECTION,
                        LOGOUT_SECTION,
                        SETTINGS_SECTION,
                        EXIT};
            }
        };
    }

    @Provides
    @IntoMap
    @HubCommands
    @StringKey(TODOS_SECTION)
    public Command provideGoToTodosSectionCommand(Provider<TodosComponent.Builder> provider) {
        return new GoToSectionCommand<TodosComponent.Builder>(provider) {
            @Override
            public String name() {
                return TODOS_SECTION;
            }
        };
    }

    @Provides
    @IntoMap
    @HubCommands
    @StringKey(ACCOUNT_SECTION)
    public Command provideGoToAccountSectionCommand(Provider<AccountComponent.Builder> provider) {
        return new GoToSectionCommand<AccountComponent.Builder>(provider) {
            @Override
            public String name() {
                return ACCOUNT_SECTION;
            }
        };
    }

    @Provides
    @IntoMap
    @HubCommands
    @StringKey(SETTINGS_SECTION)
    public Command provideGoToSettingsSectionCommand(Provider<SettingsComponent.Builder> provider) {
        return new GoToSectionCommand<SettingsComponent.Builder>(provider) {
            @Override
            public String name() {
                return SETTINGS_SECTION;
            }
        };
    }

    @Provides
    @IntoMap
    @HubCommands
    @StringKey(LOGOUT_SECTION)
    public Command provideGoToLoginSectionCommand(Provider<AuthComponent.Builder> provider) {
        return new LogoutCommand(provider);
    }

    @Provides
    @IntoMap
    @HubCommands
    @StringKey(EXIT)
    public Command provideExitCommand(View view, Closeable closeable) {
        return new SystemExitCommand(view, closeable);
    }
}
