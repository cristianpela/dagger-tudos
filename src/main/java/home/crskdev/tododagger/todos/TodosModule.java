package home.crskdev.tododagger.todos;

import com.rethinkdb.net.Connection;
import dagger.Module;
import dagger.Provides;
import dagger.multibindings.IntoMap;
import dagger.multibindings.StringKey;
import home.crskdev.tododagger.auth.LogoutCommand;
import home.crskdev.tododagger.auth.di.AuthComponent;
import home.crskdev.tododagger.auth.di.Principal;
import home.crskdev.tododagger.core.Init;
import home.crskdev.tododagger.core.LazyCache;
import home.crskdev.tododagger.core.command.*;
import home.crskdev.tododagger.core.qualifiers.InMemory;
import home.crskdev.tododagger.core.qualifiers.Proxy;
import home.crskdev.tododagger.core.qualifiers.Rethink;
import home.crskdev.tododagger.core.view.View;
import home.crskdev.tododagger.hub.ExitToHubCommand;
import home.crskdev.tododagger.hub.di.HubComponent;
import home.crskdev.tododagger.core.command.HelpCommand;
import home.crskdev.tododagger.todos.todo.*;
import home.crskdev.tododagger.todos.todo.model.*;

import javax.inject.Named;
import javax.inject.Provider;
import java.io.Closeable;
import java.util.List;
import java.util.Map;

import static home.crskdev.tododagger.auth.LogoutCommand.LOGOUT_SECTION;
import static home.crskdev.tododagger.core.command.SystemExitCommand.EXIT;
import static home.crskdev.tododagger.core.command.WelcomeCommand.WELCOME;
import static home.crskdev.tododagger.hub.ExitToHubCommand.HUB_SECTION;
import static home.crskdev.tododagger.core.command.HelpCommand.HELP;
import static home.crskdev.tododagger.todos.todo.AddCommand.ADD;
import static home.crskdev.tododagger.todos.todo.CompleteCommand.COMPLETE;
import static home.crskdev.tododagger.todos.todo.EditCommand.EDIT;
import static home.crskdev.tododagger.todos.todo.ListCommand.TODOS;
import static home.crskdev.tododagger.todos.todo.RemoveCommand.REMOVE;

/**
 * Created by criskey on 18/3/2017.
 */
@Module
public class TodosModule {

    @Provides
    @Named("todos-entry-point")
    public Init provideEntryPoint(View view, @TodoCommands Map<String, Command> commands,
                                  ListenerSwapper setter) {
        return new CommandManager(view, commands, setter);
    }

    @Provides
    @TodoScope
    @InMemory
    public ITodoService provideInMemoryTodoService(@Principal String principal, Map<String, List<Todo>> usersTodos) {
        return new InMemoryTodoService(principal, usersTodos);
    }

    @Provides
    @TodoScope
    @Rethink
    public ITodoService provideRethinkTodoService(@Principal String principal, LazyCache<Connection> lazyConnnection) {
        return new RethinkTodoService(principal, lazyConnnection);
    }

    @Provides
    @TodoScope
    @Proxy
    public ITodoService provideProxyTodoService(@Rethink ITodoService delegate) {
        return new TodoServiceProxy(delegate);
    }


    //########### Command Mapping############

    @Provides
    @IntoMap
    @TodoCommands
    @StringKey(WELCOME)
    public static Command provideWelcomeCommand(View view, @Principal String principal) {
        return new WelcomeCommand(view, principal) {
            @Override
            public String[] availableCommands() {
                return new String[]{
                        HUB_SECTION,
                        LOGOUT_SECTION,
                        EXIT,
                        HELP,
                        ADD,
                        COMPLETE,
                        EDIT,
                        TODOS,
                        REMOVE
                };
            }
        };
    }

    @Provides
    @IntoMap
    @TodoCommands
    @StringKey(HUB_SECTION)
    public Command provideHubExitCommand(Provider<HubComponent.Builder> provider) {
        return new ExitToHubCommand(provider);
    }

    @Provides
    @IntoMap
    @TodoCommands
    @StringKey(EXIT)
    public Command provideExitCommand(View view, Closeable closeable) {
        return new SystemExitCommand(view, closeable);
    }

    @Provides
    @IntoMap
    @TodoCommands
    @StringKey(LOGOUT_SECTION)
    public Command provideGoToLoginSectionCommand(Provider<AuthComponent.Builder> provider) {
        return new LogoutCommand(provider);
    }


    @Provides
    @IntoMap
    @TodoCommands
    @StringKey(HELP)
    public Command provideHelpCommand(View view) {
        return new HelpCommand(view){
            @Override
            public String helpFileName() {
                return "todo-help";
            }
        };
    }


    @Provides
    @IntoMap
    @TodoCommands
    @StringKey(ADD)
    public Command provideAddCommand(View view, @Proxy ITodoService service) {
        return new AddCommand(view, service);
    }


    @Provides
    @IntoMap
    @TodoCommands
    @StringKey(COMPLETE)
    public Command provideCompleteCommand(View view, @Proxy ITodoService service) {
        return new CompleteCommand(view, service);
    }

    @Provides
    @IntoMap
    @TodoCommands
    @StringKey(EDIT)
    public Command provideEditCommand(View view, @Proxy ITodoService service) {
        return new EditCommand(view, service);
    }

    @Provides
    @IntoMap
    @TodoCommands
    @StringKey(TODOS)
    public Command provideListCommand(View view, @Proxy ITodoService service) {
        return new ListCommand(view, service);
    }


    @Provides
    @IntoMap
    @TodoCommands
    @StringKey(REMOVE)
    public Command provideRemoveCommand(View view, @Proxy ITodoService service) {
        return new RemoveCommand(view, service);
    }
}
