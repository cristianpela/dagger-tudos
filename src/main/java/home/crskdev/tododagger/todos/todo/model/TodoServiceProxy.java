package home.crskdev.tododagger.todos.todo.model;

import java.util.Collection;

/**
 * Created by criskey on 3/4/2017.
 */
public class TodoServiceProxy implements ITodoService {

    private ITodoService delegate;

    public TodoServiceProxy(ITodoService delegate) {
        this.delegate = delegate;
    }

    @Override
    public Collection<Todo> findAll() throws TodoException {
        return delegate.findAll();
    }

    @Override
    public Collection<Todo> find(Integer... ids) throws TodoException {
        return delegate.find(ids);
    }

    @Override
    public Collection<Integer> findAllIds() throws TodoException {
        return delegate.findAllIds();
    }

    @Override
    public String add(String task) throws TodoException {
        return delegate.add(task);
    }

    @Override
    public void remove(Integer... ids) throws TodoException {
        delegate.remove(ids);
    }

    @Override
    public boolean removeAll() throws TodoException {
        return delegate.removeAll();
    }

    @Override
    public void complete(boolean completed, Integer... ids) throws TodoException {
        delegate.complete(completed, ids);
    }

    @Override
    public boolean completeAll(boolean complete) throws TodoException {
        return delegate.completeAll(complete);
    }

    @Override
    public void edit(int id, String newTask) throws TodoException {
        delegate.edit(id, newTask);
    }
}
