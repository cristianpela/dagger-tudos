package home.crskdev.tododagger.todos.todo;

import home.crskdev.tododagger.core.command.CommandException;
import home.crskdev.tododagger.todos.todo.model.TodoException;

import java.util.List;

/**
 * Created by criskey on 16/3/2017.
 */
public final class TodoUtils {

    public static int checkIdValidity(String strId) throws CommandException {
        int id;
        try {
            id = Integer.parseInt(strId.trim());
            if (id < 0) {
                throw new CommandException("Todo id must be a positive integer number");
            }
        } catch (NumberFormatException e) {
            throw new CommandException("Todo id must be a positive integer number");
        }
        return id;
    }

    public static Integer[] checkBatchIdValidity(String strId) throws CommandException {
        String[] strIds = strId.split("-");
        Integer[] ids = new Integer[strIds.length];
        for (int i = 0; i < strIds.length; i++) {
            int id = checkIdValidity(strIds[i]);
            ids[i] = id;
        }
        return ids;
    }

    public static TodoException generateException(String template, List<Integer> ids) {
        if (ids.size() == 1) {
            return new TodoException(template + ids.get(0));
        }
        StringBuilder builder = new StringBuilder();
        for (int i = 0, s = ids.size(); i < s; i++) {
            String delim = (i < s - 1) ? ", " : "";
            builder.append(ids.get(i));
            builder.append(delim);
        }
        return new TodoException(template + builder.toString());
    }
}
