package home.crskdev.tododagger.todos.todo;

import home.crskdev.tododagger.core.command.CommandException;
import home.crskdev.tododagger.core.view.View;
import home.crskdev.tododagger.todos.todo.model.ITodoService;
import home.crskdev.tododagger.todos.todo.model.TodoException;

import javax.inject.Inject;

/**
 * Created by criskey on 16/3/2017.
 */
public class EditCommand extends BaseTodoCommand {

    public static final String EDIT = "edit";

    @Inject
    public EditCommand(View view, ITodoService service) {
        super(view, service);
    }

    @Override
    public void execute(String... args) throws CommandException {
        if (args.length < 3) {
            throw new CommandException("Must provide 2 arguments. Example: edit <id> <your content>");
        } else {
            int id = TodoUtils.checkIdValidity(args[1]);
            StringBuilder updateTask = new StringBuilder();
            for (int i = 2; i < args.length; i++) {
                updateTask.append(args[i]);
                if ((i < args.length - 1)) {
                    updateTask.append(" ");
                }
            }
            try {
                service.edit(id, updateTask.toString());
                view.println("Todo with id " + id + " was updated.");
            } catch (TodoException e) {
                throw new CommandException(e.getMessage());
            }
        }
    }

    @Override
    public String name() {
        return EDIT;
    }

    @Override
    public boolean isDefault() {
        return false;
    }
}
