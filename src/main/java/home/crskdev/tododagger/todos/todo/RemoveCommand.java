package home.crskdev.tododagger.todos.todo;

import home.crskdev.tododagger.core.command.CommandException;
import home.crskdev.tododagger.core.view.View;
import home.crskdev.tododagger.todos.todo.model.ITodoService;
import home.crskdev.tododagger.todos.todo.model.TodoException;

import javax.inject.Inject;

/**
 * Created by criskey on 16/3/2017.
 */
public class RemoveCommand extends BaseTodoCommand {

    public static final String REMOVE = "remove";

    @Inject
    public RemoveCommand(View view, ITodoService service) {
        super(view, service);
    }

    @Override
    public void execute(String... args) throws CommandException {
        if (args.length != 2) {
            throw new CommandException("Must have one argument. Example: remove <id>");
        } else {
            String id = args[1].trim().toLowerCase();
            if (id.equals("all")) {
                try {
                    service.removeAll();
                    view.println("All todos removed");
                } catch (TodoException e) {
                    throw new CommandException(e.getMessage());
                }
            } else {
                Integer[] ids = TodoUtils.checkBatchIdValidity(id);
                try {
                    remove(id, ids);
                } catch (CommandException e) {
                    throw new CommandException(e.getMessage());
                }
            }
        }
    }

    private void remove(String id, Integer[] ids) throws CommandException {
        try {
            service.remove(ids);
            if (ids.length == 1) {
                view.println("Todo with id " + id + " removed.");
            } else {
                view.println("Todo batch was removed ");
            }
        } catch (TodoException e) {
            throw new CommandException(e.getMessage());
        }

    }

    @Override
    public String name() {
        return REMOVE;
    }

    @Override
    public boolean isDefault() {
        return false;
    }
}
