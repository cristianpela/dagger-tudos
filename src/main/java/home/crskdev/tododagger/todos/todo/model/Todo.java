package home.crskdev.tododagger.todos.todo.model;

import java.util.Date;

/**
 * Created by criskey on 14/3/2017.
 */
public class Todo {

    private static int ID_GEN = 0;

    public final int id;

    public final long date;

    public final String task;

    public final boolean completed;

    public Todo(long date, String task, boolean completed) {
        this(++ID_GEN, date, task, completed);
    }

    public Todo(long date, String task) {
        this(++ID_GEN, date, task, false);
    }

    public Todo(String task) {
        this(++ID_GEN, task);
    }

    //for testing
    public Todo(int id, String task) {
        this(id, new Date().getTime(), task, false);
    }

    public Todo(int id, long date, String task, boolean completed) {
        this.date = date;
        this.task = task;
        this.completed = completed;
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Todo todo = (Todo) o;

        return id == todo.id;

    }

    @Override
    public String toString() {
        return "Todo{" +
                "id=" + id +
                ", date=" + date +
                ", task='" + task + '\'' +
                ", completed=" + completed +
                '}';
    }

    @Override
    public int hashCode() {
        return id;
    }
}
