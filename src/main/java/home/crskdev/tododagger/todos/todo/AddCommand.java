package home.crskdev.tododagger.todos.todo;

import home.crskdev.tododagger.core.command.CommandException;
import home.crskdev.tododagger.core.view.View;
import home.crskdev.tododagger.todos.todo.model.ITodoService;
import home.crskdev.tododagger.todos.todo.model.TodoException;

import javax.inject.Inject;

/**
 * Created by criskey on 14/3/2017.
 */
public class AddCommand extends BaseTodoCommand {

    public static final String ADD = "add";

    @Inject
    public AddCommand(View view, ITodoService service) {
        super(view, service);
    }

    @Override
    public void execute(String... args) throws CommandException {
        if (args.length == 1) {
            throw new CommandException("The home.crskdev.tododagger.core.todo is empty");
        } else {
            StringBuilder newTask = new StringBuilder();
            for (int i = 1; i < args.length; i++) {
                newTask.append(args[i]);
                if ((i < args.length - 1)) {
                    newTask.append(" ");
                }
            }
            String id = null;
            try {
                id = service.add(newTask.toString());
            } catch (TodoException e) {
                throw new CommandException(e.getMessage());
            }
            view.println("Todo with id " + id + " was created.");
        }
    }

    @Override
    public String name() {
        return ADD;
    }

}
