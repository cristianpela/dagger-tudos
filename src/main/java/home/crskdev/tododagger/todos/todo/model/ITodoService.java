package home.crskdev.tododagger.todos.todo.model;

import java.util.Collection;

/**
 * Created by criskey on 17/3/2017.
 */
public interface ITodoService {

    Collection<Todo> findAll() throws TodoException;

    Collection<Todo> find(Integer... ids) throws TodoException;

    Collection<Integer> findAllIds() throws TodoException;

    /**
     * Creates a new _todo
     *
     * @param task
     * @return id of the newly created _todo
     */
    String add(String task) throws TodoException;

    /**
     * Removes a batch of ids
     * @param ids
     */
    void remove(Integer... ids) throws TodoException;

    boolean removeAll() throws TodoException;

    /**
     * mark a batch of todos completed or in-completed
     * @param completed
     * @param ids
     */
    void complete(boolean completed, Integer... ids) throws TodoException;

    boolean completeAll(boolean complete) throws TodoException;

    void edit(int id, String newTask) throws TodoException;

}
