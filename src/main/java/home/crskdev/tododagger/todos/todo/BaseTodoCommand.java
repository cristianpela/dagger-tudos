package home.crskdev.tododagger.todos.todo;

import home.crskdev.tododagger.core.command.Command;
import home.crskdev.tododagger.core.command.CommandException;
import home.crskdev.tododagger.core.command.ViewCommand;
import home.crskdev.tododagger.core.view.View;
import home.crskdev.tododagger.todos.todo.model.ITodoService;

/**
 * Created by criskey on 16/3/2017.
 */
public abstract class BaseTodoCommand extends ViewCommand {

    protected ITodoService service;

    public BaseTodoCommand(View view, ITodoService service) {
        super(view);
        this.service = service;
    }

    @Override
    public void execute(String... args) throws CommandException {
        if (!args[0].equals(name())) {
            //usually this case is already handled by CommandManager.
            //so, in prod, calling this method by subclasses should be redundant
            throw new CommandException("Command " + args[0] + " unkwnon");
        }
    }

    @Override
    public boolean isDefault() {
        return false;
    }

}
