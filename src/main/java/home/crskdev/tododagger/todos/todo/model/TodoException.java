package home.crskdev.tododagger.todos.todo.model;

/**
 * Created by criskey on 21/3/2017.
 */
public class TodoException extends RuntimeException {

    public TodoException() {
        super();
    }

    public TodoException(String message) {
        super(message);
    }

    public TodoException(String message, Throwable cause) {
        super(message, cause);
    }

    public TodoException(Throwable cause) {
        super(cause);
    }

    protected TodoException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
