package home.crskdev.tododagger.todos.todo.model;

import com.rethinkdb.gen.ast.Filter;
import com.rethinkdb.gen.ast.Table;
import com.rethinkdb.net.Connection;
import com.rethinkdb.net.Cursor;
import home.crskdev.tododagger.core.LazyCache;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.rethinkdb.RethinkDB.r;
import static home.crskdev.tododagger.todos.todo.TodoUtils.generateException;

/**
 * Created by criskey on 1/4/2017.
 */
public class RethinkTodoService implements ITodoService {

    public static final String DATE = "date";
    public static final String TASK = "task";
    public static final String COMPLETE = "complete";
    public static final String NAME = "name";
    public static final String KEY_ID = "id";

    private LazyCache<Connection> lazyCon;

    private String principal;

    private List<String> todoKeys;

    public RethinkTodoService(String principal, LazyCache<Connection> lazyCon) {
        this.lazyCon = lazyCon;
        this.principal = principal;
        this.todoKeys = new ArrayList<>();
    }

    @Override
    public Collection<Todo> findAll() throws TodoException {
        ArrayList<HashMap<String, ?>> result;
        try {
            result = byPrincipal()
                    .orderBy(r.desc(DATE))
                    .run(lazyCon.get());
        } catch (Exception e) {
            throw new TodoException(e.getMessage());
        }
        return getTodos(result.iterator());
    }

    @Override
    public Collection<Todo> find(Integer... ids) throws TodoException {
        //ids start from one not 0;
        checkAllKeys();
        Object[] keys = Arrays.stream(ids)
                .map(id -> (id - 1 >= 0 && id - 1 < todoKeys.size()) ? todoKeys.get(id - 1) : null)
                .filter(idKey -> idKey != null).toArray();
        ArrayList<HashMap<String, ?>> result;
        try {
            result = byPrincipal()
                    .orderBy(r.desc(DATE))
                    .filter((todo) -> r.expr(keys).contains(todo.g(KEY_ID)))
                    .run(lazyCon.get());
        } catch (Exception e) {
            throw new TodoException(e.getMessage());
        }
        return getTodos(result.iterator());
    }


    @Override
    public Collection<Integer> findAllIds() throws TodoException {
        throw new UnsupportedOperationException();
    }

    @Override
    public String add(String task) throws TodoException {
        HashMap<String, ?> result;
        try {
            result = table().insert(r.hashMap(NAME, principal)
                    .with(DATE, System.currentTimeMillis())
                    .with(COMPLETE, false)
                    .with(TASK, task))
                    .run(lazyCon.get());
        } catch (Exception e) {
            throw new TodoException(e.getMessage());
        }
        return ((ArrayList<String>) result.get("generated_keys")).get(0);
    }

    @Override
    public void remove(Integer... ids) throws TodoException {
        checkAllKeys();
        int size = todoKeys.size();
        List<Integer> notFoundIds = new ArrayList<>();
        List<String> toRemove = Stream.of(ids)
                .map(id -> {
                    String ret = null;
                    if (id <= size) {
                        ret = todoKeys.get(id - 1);
                    } else {
                        notFoundIds.add(id);
                    }
                    return ret;
                })
                .filter(id -> id != null)
                .collect(Collectors.toList());
        try {
            for (String key : toRemove) {
                table().get(key).delete()
                        .run(lazyCon.get());
                todoKeys.remove(key);
            }
            if (!notFoundIds.isEmpty()) {
                if (toRemove.isEmpty()) {
                    throw new TodoException("No todos were found to remove");
                } else {
                    throw generateException("Todos were removed except ids: ", notFoundIds);
                }
            }
        } catch (Exception e) {
            throw new TodoException(e.getMessage());
        }

    }

    @Override
    public boolean removeAll() throws TodoException {
        try {
            byPrincipal().delete().run(lazyCon.get());
            todoKeys.clear();
        } catch (Exception e) {
            throw new TodoException(e.getMessage());
        }
        return true;
    }

    @Override
    public void complete(boolean completed, Integer... ids) throws TodoException {
        checkAllKeys();
        int size = todoKeys.size();
        List<Integer> notFoundIds = new ArrayList<>();
        try {
            for (int id : ids) {
                if (id <= size) {
                    String key = todoKeys.get(id - 1);
                    table().get(key)
                            .update(r.hashMap(COMPLETE, completed))
                            .run(lazyCon.get());
                } else {
                    notFoundIds.add(id);
                }
            }
            if (!notFoundIds.isEmpty()) {
                throw generateException("Todos marked completed except ids: ", notFoundIds);
            }
        } catch (Exception e) {
            throw new TodoException(e.getMessage());
        }
    }

    @Override
    public boolean completeAll(boolean completed) throws TodoException {
        try {
            byPrincipal().update(r.hashMap(COMPLETE, completed))
                    .run(lazyCon.get());
        } catch (Exception e) {
            throw new TodoException(e.getMessage());
        }
        return true;
    }

    @Override
    public void edit(int id, String newTask) throws TodoException {
        checkAllKeys();
        if (id <= todoKeys.size()) {
            String key = todoKeys.get(id - 1);
            table().get(key).update(r.hashMap(TASK, newTask))
                    .run(lazyCon.get());
        } else {
            throw new TodoException("Invalid id " + id);
        }
    }

    //#### helper methods#####

    private Table table() {
        return r.db("test").table("todos");
    }

    private Filter byPrincipal() {
        return table().filter(todo -> todo.g(NAME).eq(principal));
    }

    private List<Todo> getTodos(Iterator<HashMap<String, ?>> it) throws TodoException {
        todoKeys.clear();
        List<Todo> todos = new ArrayList<>();
        boolean isEmpty = true;
        int idGen = 1;
        while (it.hasNext()) {
            isEmpty = false;
            HashMap<String, ?> values = it.next();
            long date = Long.parseLong(values.get(DATE).toString());
            String task = (String) values.get(TASK.toString().toString());
            boolean complete = Boolean.parseBoolean(values.get(COMPLETE).toString());
            todos.add(new Todo(idGen++, date, task, complete));
            todoKeys.add(values.get(KEY_ID).toString());
        }
        if (isEmpty)
            throw new TodoException("No todos found");
        return todos;
    }

    private void checkAllKeys() {
        if (todoKeys.isEmpty()) {
            Cursor<?> cursor = byPrincipal().pluck("id").run(lazyCon.get());
            while (cursor.hasNext()) {
                String next = ((HashMap<String, ?>) cursor.next()).get("id").toString();
                todoKeys.add(next);
            }
        }
    }
}
