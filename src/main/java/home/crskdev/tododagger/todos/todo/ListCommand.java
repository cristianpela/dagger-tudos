package home.crskdev.tododagger.todos.todo;

import home.crskdev.tododagger.core.command.CommandException;
import home.crskdev.tododagger.core.view.View;
import home.crskdev.tododagger.core.view.table.Alignment;
import home.crskdev.tododagger.core.view.table.DividersSpec;
import home.crskdev.tododagger.core.view.table.RenderedCell;
import home.crskdev.tododagger.core.view.table.Table;
import home.crskdev.tododagger.core.view.table.schema.BooleanSchema;
import home.crskdev.tododagger.core.view.table.schema.DateSchema;
import home.crskdev.tododagger.core.view.table.schema.StringSchema;
import home.crskdev.tododagger.todos.todo.model.ITodoService;
import home.crskdev.tododagger.todos.todo.model.Todo;
import home.crskdev.tododagger.todos.todo.model.TodoException;

import javax.inject.Inject;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;

import static home.crskdev.tododagger.core.view.ANSI.Color.*;

/**
 * Created by criskey on 13/3/2017.
 */
public class ListCommand extends BaseTodoCommand {

    public static final String TODOS = "todos";

    private DateFormat dateFormat;

    @Inject
    public ListCommand(View view, ITodoService service) {
        super(view, service);
        dateFormat = new SimpleDateFormat("dd-MM-yyy hh:mm:ss");
    }

    @Override
    public void execute(String... cmd) throws CommandException {
        Table table = new Table(view,
                new DividersSpec(MAGENTA),
                new StringSchema(5),
                new DateSchema(21),
                new StringSchema(35),
                new BooleanSchema(11));
        //we center the ID column
        table.addCellRenderer((row, col, cell) -> {
            if (col == 0 && row > 0) {
                String id = cell.content.toString();
                return new RenderedCell(cell.schema.width,
                        Alignment.Type.CENTER,
                        id + ").");
            }
            return null;
        });
        //we make completed cells wih background green and red those which are aren't
        table.addCellDecorator((row, col, decorator, cell) -> {
            if (!cell.isHeader) {
                if (col == 3) {
                    return decorator
                            .background(((Boolean) cell.content).booleanValue() ? GREEN : RED)
                            .foreground(WHITE)
                            .create();
                } else if (col == 0) {
                    return decorator
                            .background(YELLOW)
                            .foreground(RED)
                            .create();
                }
            }
            return null;
        });

        table.addHeaderRow("Id", "Date", "Name", "Completed");

        Collection<Todo> all;
        try {
            all = service.findAll();
        } catch (TodoException e) {
            throw new CommandException(e.getMessage());
        }
        for (Todo todo : all) {
            table.addRow(Integer.toString(todo.id),
                    new Date(todo.date),
                    todo.task,
                    todo.completed);
        }
        table.display();
    }

    @Override
    public String name() {
        return TODOS;
    }

    @Override
    public boolean isDefault() {
        return false;
    }
}
