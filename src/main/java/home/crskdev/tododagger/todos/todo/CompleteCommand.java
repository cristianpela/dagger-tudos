package home.crskdev.tododagger.todos.todo;

import home.crskdev.tododagger.core.command.CommandException;
import home.crskdev.tododagger.core.view.View;
import home.crskdev.tododagger.todos.todo.model.ITodoService;
import home.crskdev.tododagger.todos.todo.model.TodoException;

import javax.inject.Inject;

/**
 * Created by criskey on 16/3/2017.
 */
public class CompleteCommand extends BaseTodoCommand {

    public static final String COMPLETE = "complete";

    @Inject
    public CompleteCommand(View view, ITodoService service) {
        super(view, service);
    }

    @Override
    public void execute(String... args) throws CommandException {
        if (args.length > 3 || args.length < 2) {
            throw new CommandException("Must provide two or three arguments. " +
                    "\nExample: complete <id> <'yes'|'no'> or complete <id>");
        } else {
            String strId = args[1].toLowerCase().trim();
            //TODO ugly code ahead
            if (strId.equals("all")) {
                if (args.length == 3) {
                    String completedStr = args[2].toLowerCase().trim();
                    if (completedStr.equals("yes") || completedStr.equals("no")) {
                        if (!service.completeAll(completedStr.equals("yes"))) {
                            throw new CommandException("Could not complete all todos. Try later");
                        } else {
                            view.println("All todos were marked as completed");
                        }
                    } else {
                        throw new CommandException("The second argument must be 'yes' or 'no'");
                    }
                } else {
                    try {
                        if (!service.completeAll(true)) {
                            throw new CommandException("Could not complete all todos. Try later");
                        } else {
                            view.println("All todos were marked as completed");
                        }
                    } catch (TodoException e) {
                        throw new CommandException(e.getMessage());
                    }
                }
            } else {
                Integer[] ids = TodoUtils.checkBatchIdValidity(strId);
                if (args.length == 3) {
                    String completedStr = args[2].toLowerCase().trim();
                    if (completedStr.equals("yes") || completedStr.equals("no")) {
                        completeBatch(completedStr.equals("yes"), ids);
                    } else {
                        throw new CommandException("The second argument must be 'yes' or 'no'");
                    }
                } else {
                    completeBatch(true, ids);
                }
            }
        }
    }

    private void completeBatch(boolean complete, Integer... ids) throws CommandException {
        try {
            service.complete(complete, ids);
            String completedStr = (complete) ? "completed" : "uncompleted";
            if (ids.length > 1) {
                view.println("Todo batch was marked as " + completedStr);
            } else {
                view.println("Todo with id " + ids[0] + " was mark as " + completedStr);
            }
        } catch (TodoException e) {
            throw new CommandException(e.getMessage());
        }
    }

    @Override
    public String name() {
        return COMPLETE;
    }

    @Override
    public boolean isDefault() {
        return false;
    }
}
