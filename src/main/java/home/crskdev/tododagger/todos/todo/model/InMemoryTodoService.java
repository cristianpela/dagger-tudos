package home.crskdev.tododagger.todos.todo.model;

import home.crskdev.tododagger.todos.todo.TodoUtils;

import javax.inject.Inject;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by criskey on 14/3/2017.
 */
public class InMemoryTodoService implements ITodoService {

    private final Map<String, List<Todo>> usersTodos;

    private String principal;

    @Inject
    public InMemoryTodoService(String principal, Map<String, List<Todo>> usersTodos) {
        this.usersTodos = usersTodos;
        this.principal = principal;
        if (usersTodos.get(principal) == null) {
            usersTodos.put(principal, new ArrayList<>());
        }
    }

    @Override
    public Collection<Todo> findAll() {
        return Collections.unmodifiableList(usersTodos.get(principal));
    }

    @Override
    public Collection<Todo> find(Integer... ids) {
        List<Todo> some = new ArrayList<>();
        if (ids.length == 1) {
            IndexedTodo idxtodo = findIndexedOne(ids[0]);
            if (idxtodo != null) {
                some.add(idxtodo.todo);
            }
        } else {
            for (int id : ids) {
                IndexedTodo idxtodo = findIndexedOne(id);
                if (idxtodo != null) {
                    some.add(idxtodo.todo);
                }
            }
            //sort asc
            Collections.sort(some, (o1, o2)
                    -> o1.id > o2.id ? 1 : o1.id < o2.id ? -1 : 0);
        }
        return some;
    }

    @Override
    public Collection<Integer> findAllIds() {
        return usersTodos.get(principal).stream()
                .map((todo) -> todo.id)
                .collect(Collectors.toList());
    }

    @Override
    public String add(String task) {
        Todo todo = new Todo(task);
        usersTodos.get(principal).add(todo);
        return Integer.toString(todo.id);
    }

    @Override
    public void remove(Integer... ids) {
        List<Integer> notFoundIds = new ArrayList<>();
        for (int id : ids) {
            IndexedTodo found = findIndexedOne(id);
            boolean exists = found != null;
            if (exists) {
                usersTodos.get(principal).remove(found.index);
            } else {
                notFoundIds.add(id);
            }
        }
        if (!notFoundIds.isEmpty()) {
            throw TodoUtils.generateException("Todos removed except ids: ", notFoundIds);
        }
    }

    @Override
    public boolean removeAll() {
        usersTodos.get(principal).clear();
        return usersTodos.get(principal).isEmpty();
    }


    @Override
    public void complete(boolean completed, Integer... ids) {
        List<Integer> notFoundIds = new ArrayList<>();
        for (int id : ids) {
            IndexedTodo found = findIndexedOne(id);
            boolean exists = found != null;
            if (exists) {
                Todo updatedTodo = new Todo(
                        found.todo.id,
                        found.todo.date,
                        found.todo.task,
                        completed);
                usersTodos.get(principal).set(found.index, updatedTodo);
            } else {
                notFoundIds.add(id);
            }
        }
        if (!notFoundIds.isEmpty()) {
            String complStr = completed ? "complete" : "incomplete";
            throw TodoUtils.generateException("Todos were marked as " + complStr + " except ids: ", notFoundIds);
        }
    }

    @Override
    public boolean completeAll(boolean complete) {
        List<Todo> todos = this.usersTodos.get(principal);
        for (int i = 0; i < todos.size(); i++) {
            Todo oldTodo = todos.get(i);
            todos.set(i, new Todo(oldTodo.id, oldTodo.date, oldTodo.task, complete));
        }
        return true;
    }

    @Override
    public void edit(int id, String newTask) {
        IndexedTodo indexedTodo = findIndexedOne(id);
        boolean exist = indexedTodo != null;
        if (exist) {
            usersTodos.get(principal).set(indexedTodo.index,
                    new Todo(indexedTodo.todo.id,
                            indexedTodo.todo.date,
                            newTask,
                            indexedTodo.todo.completed));
        } else {
            throw new TodoException("Todo with id " + id + " not found");
        }
    }

    private IndexedTodo findIndexedOne(int id) {
        List<Todo> todos = this.usersTodos.get(principal);
        for (int i = 0; i < todos.size(); i++) {
            Todo curr = todos.get(i);
            if (curr.id == id) {
                return new IndexedTodo(i, curr);
            }
        }
        return null;
    }


    private static class IndexedTodo {
        final int index;
        final Todo todo;

        IndexedTodo(int index, Todo todo) {
            this.index = index;
            this.todo = todo;
        }
    }
}
