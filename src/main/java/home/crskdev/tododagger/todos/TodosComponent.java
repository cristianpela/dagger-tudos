package home.crskdev.tododagger.todos;

import dagger.Subcomponent;
import home.crskdev.tododagger.core.Init;
import home.crskdev.tododagger.core.InitComponent;

import javax.inject.Named;

/**
 * Created by criskey on 18/3/2017.
 */
@TodoScope
@Subcomponent(modules = TodosModule.class)
public interface TodosComponent extends InitComponent {

    @Override
    @Named("todos-entry-point")
    Init init();

    @Subcomponent.Builder
    interface Builder extends InitComponent.Builder<TodosComponent> {
        TodosComponent build();
    }
}
