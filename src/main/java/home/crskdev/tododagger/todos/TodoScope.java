package home.crskdev.tododagger.todos;

import javax.inject.Scope;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created by criskey on 18/3/2017.
 */

@Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface TodoScope {}
