package home.crskdev.tododagger.account;

import home.crskdev.tododagger.auth.model.AuthException;
import home.crskdev.tododagger.auth.model.IAuthService;
import home.crskdev.tododagger.auth.model.User;
import home.crskdev.tododagger.core.command.Command;
import home.crskdev.tododagger.core.command.CommandException;
import home.crskdev.tododagger.core.command.ViewCommand;
import home.crskdev.tododagger.core.view.View;

/**
 * Created by criskey on 21/3/2017.
 */
public class PasswordChangeCommand extends ViewCommand {

    public static final String NEW_PASSWORD = "newpass";

    private final String principal;

    private IAuthService authService;

    public PasswordChangeCommand(View view, String principal, IAuthService authService) {
        super(view);
        this.principal = principal;
        this.authService = authService;
    }

    @Override
    public void execute(String... args) throws CommandException {
        if (args.length != 3) {
            throw new CommandException("Wrong number of arguments. " +
                    "Should be like: " + NEW_PASSWORD + " <oldpassword> <newpassword>");
        }
        char[] oldPassword = args[1].trim().toCharArray();
        char[] newPassword = args[2].trim().toCharArray();
        try {
            authService.changePassword(new User(principal, oldPassword), newPassword);
            view.println("Password has been changed");
        } catch (AuthException ex) {
            throw new CommandException(ex.getMessage());
        }
    }

    @Override
    public String name() {
        return NEW_PASSWORD;
    }

    @Override
    public boolean isDefault() {
        return false;
    }

}
