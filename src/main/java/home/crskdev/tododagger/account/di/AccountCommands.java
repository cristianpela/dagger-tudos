package home.crskdev.tododagger.account.di;

import javax.inject.Qualifier;

/**
 * Created by criskey on 21/3/2017.
 */
@Qualifier
public @interface AccountCommands {
}
