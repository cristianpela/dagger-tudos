package home.crskdev.tododagger.account.di;

import dagger.Module;
import dagger.Provides;
import dagger.multibindings.IntoMap;
import dagger.multibindings.StringKey;
import home.crskdev.tododagger.account.AboutCommand;
import home.crskdev.tododagger.account.PasswordChangeCommand;
import home.crskdev.tododagger.account.details.di.DetailsComponent;
import home.crskdev.tododagger.auth.LogoutCommand;
import home.crskdev.tododagger.auth.di.AuthComponent;
import home.crskdev.tododagger.auth.di.Principal;
import home.crskdev.tododagger.auth.model.IAuthService;
import home.crskdev.tododagger.core.Init;
import home.crskdev.tododagger.core.command.*;
import home.crskdev.tododagger.core.qualifiers.Proxy;
import home.crskdev.tododagger.core.view.View;
import home.crskdev.tododagger.hub.ExitToHubCommand;
import home.crskdev.tododagger.hub.di.HubComponent;

import javax.inject.Named;
import javax.inject.Provider;
import java.io.Closeable;
import java.util.Map;

import static home.crskdev.tododagger.account.AboutCommand.ABOUT;
import static home.crskdev.tododagger.account.PasswordChangeCommand.NEW_PASSWORD;
import static home.crskdev.tododagger.auth.LogoutCommand.LOGOUT_SECTION;
import static home.crskdev.tododagger.core.command.SystemExitCommand.EXIT;
import static home.crskdev.tododagger.core.command.WelcomeCommand.WELCOME;
import static home.crskdev.tododagger.hub.ExitToHubCommand.HUB_SECTION;

/**
 * Created by criskey on 21/3/2017.
 */
@Module(subcomponents = DetailsComponent.class)
public class AccountModule {

    public static final String EDIT_SECTION = "edit";

    @Provides
    @Named("account-entry-point")
    public Init provideEntryPoint(View view, @AccountCommands Map<String, Command> commands,
                                  ListenerSwapper setter) {
        return new CommandManager(view, commands, setter);
    }

    //##### Command Mapping ####

    @Provides
    @IntoMap
    @AccountCommands
    @StringKey(NEW_PASSWORD)
    public Command provideNewPasswordCommand(View view, @Principal String principal,@Proxy IAuthService authService) {
        return new PasswordChangeCommand(view, principal, authService);
    }

    @Provides
    @IntoMap
    @AccountCommands
    @StringKey(WELCOME)
    public Command provideWelcomeCommand(View view, @Principal String principal) {
        return new WelcomeCommand(view, principal) {
            @Override
            public String[] availableCommands() {
                return new String[]{
                        ABOUT,
                        NEW_PASSWORD,
                        HUB_SECTION,
                        LOGOUT_SECTION,
                        EXIT,
                        EDIT_SECTION
                };
            }
        };
    }

    @Provides
    @IntoMap
    @AccountCommands
    @StringKey(ABOUT)
    public Command provideAboutCommand(View view, @Proxy IAuthService authService) {
        return new AboutCommand(view, authService);
    }

    @Provides
    @IntoMap
    @AccountCommands
    @StringKey(EDIT_SECTION)
    public Command provideEditCommand(Provider<DetailsComponent.Builder> provider) {
        return new GoToSectionCommand<DetailsComponent.Builder>(provider) {
            @Override
            public String name() {
                return EDIT_SECTION;
            }
        };
    }

    @Provides
    @IntoMap
    @StringKey(HUB_SECTION)
    @AccountCommands
    public Command provideExitToHubCommand(Provider<HubComponent.Builder> provider) {
        return new ExitToHubCommand(provider);
    }

    @Provides
    @IntoMap
    @AccountCommands
    @StringKey(EXIT)
    public Command provideExitCommand(View view, Closeable closeable) {
        return new SystemExitCommand(view, closeable);
    }

    @Provides
    @IntoMap
    @AccountCommands
    @StringKey(LOGOUT_SECTION)
    public Command provideGoToLoginSectionCommand(Provider<AuthComponent.Builder> provider) {
        return new LogoutCommand(provider);
    }

}
