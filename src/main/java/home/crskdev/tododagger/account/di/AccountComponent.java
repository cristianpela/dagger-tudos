package home.crskdev.tododagger.account.di;

import dagger.Subcomponent;
import home.crskdev.tododagger.core.Init;
import home.crskdev.tododagger.core.InitComponent;

import javax.inject.Named;

/**
 * Created by criskey on 21/3/2017.
 */
@Subcomponent(modules = AccountModule.class)
public interface AccountComponent extends InitComponent {

    @Override
    @Named("account-entry-point")
    Init init();

    @Subcomponent.Builder
    interface Builder extends InitComponent.Builder<AccountComponent>{}
}
