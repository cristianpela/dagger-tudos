package home.crskdev.tododagger.account.details;

/**
 * Created by criskey on 22/3/2017.
 */
public class UserDetails {

    public static UserDetails empty(String username) {
        return new UserDetails(username, "", "", -1, "");
    }

    public final String username;

    public String firstName;

    public String lastName;

    public long birthdate;

    public String about;

    public UserDetails(String username) {
        this.username = username;
    }

    public UserDetails(String username,
                       String firstName,
                       String lastName,
                       long birthdate,
                       String about) {
        this.username = username;
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthdate = birthdate;
        this.about = about;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UserDetails that = (UserDetails) o;

        if (birthdate != that.birthdate) return false;
        if (username != null ? !username.equals(that.username) : that.username != null) return false;
        if (firstName != null ? !firstName.equals(that.firstName) : that.firstName != null) return false;
        if (lastName != null ? !lastName.equals(that.lastName) : that.lastName != null) return false;
        return about != null ? about.equals(that.about) : that.about == null;

    }

    @Override
    public int hashCode() {
        int result = username != null ? username.hashCode() : 0;
        result = 31 * result + (firstName != null ? firstName.hashCode() : 0);
        result = 31 * result + (lastName != null ? lastName.hashCode() : 0);
        result = 31 * result + (int) (birthdate ^ (birthdate >>> 32));
        result = 31 * result + (about != null ? about.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "UserDetails{" +
                "username='" + username + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", birthdate=" + birthdate +
                ", about='" + about + '\'' +
                '}';
    }


    public static class Builder {

        private final String username;
        private String firstName;
        private String lastName;
        private long birthdate;
        private String about;

        public Builder(String username) {
            this.username = username;
        }

        public Builder setFirstName(String firstName) {
            this.firstName = firstName;
            return this;
        }

        public Builder setLastName(String lastName) {
            this.lastName = lastName;
            return this;
        }

        public Builder setBirthdate(long birthdate) {
            this.birthdate = birthdate;
            return this;
        }

        public Builder setAbout(String about) {
            this.about = about;
            return this;
        }

        public UserDetails create() {
            return new UserDetails(username,
                    firstName,
                    lastName,
                    birthdate,
                    about);
        }
    }
}
