package home.crskdev.tododagger.account.details.di;

import dagger.Subcomponent;
import home.crskdev.tododagger.core.Init;
import home.crskdev.tododagger.core.InitComponent;

import javax.inject.Named;

/**
 * Created by criskey on 22/3/2017.
 */
@Subcomponent(modules = DetailsModule.class)
@DetailsScope
public interface DetailsComponent extends InitComponent{

    @Override
    @Named("details-entry-point")
    Init init();

    @Subcomponent.Builder
    interface Builder extends InitComponent.Builder<DetailsComponent>{}

}
