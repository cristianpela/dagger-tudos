package home.crskdev.tododagger.account.details.di;

/**
 * Created by criskey on 22/3/2017.
 */

import javax.inject.Scope;

@Scope
public @interface DetailsScope {
}
