package home.crskdev.tododagger.account.details.di;

import dagger.Module;
import dagger.Provides;
import dagger.multibindings.IntoMap;
import dagger.multibindings.StringKey;
import home.crskdev.tododagger.account.details.DetailsCommandManager;
import home.crskdev.tododagger.account.details.EditDetailsCommand;
import home.crskdev.tododagger.account.di.AccountComponent;
import home.crskdev.tododagger.auth.LogoutCommand;
import home.crskdev.tododagger.auth.di.AuthComponent;
import home.crskdev.tododagger.auth.model.IAuthService;
import home.crskdev.tododagger.core.Init;
import home.crskdev.tododagger.core.command.Command;
import home.crskdev.tododagger.core.command.ListenerSwapper;
import home.crskdev.tododagger.core.command.SystemExitCommand;
import home.crskdev.tododagger.core.qualifiers.Proxy;
import home.crskdev.tododagger.core.view.View;

import javax.inject.Named;
import javax.inject.Provider;
import java.io.Closeable;
import java.util.Map;

import static home.crskdev.tododagger.auth.LogoutCommand.LOGOUT_SECTION;
import static home.crskdev.tododagger.core.command.SystemExitCommand.EXIT;

/**
 * Created by criskey on 22/3/2017.
 */
@Module
public class DetailsModule {

    @Provides
    @Named("details-entry-point")
    public Init provideDetailsEntryPoint(View view, @DetailsCommands Map<String, Command> commands,
                                         ListenerSwapper setter) {
        return new DetailsCommandManager(view, commands, setter);
    }

    //########### Command Map ###########

    @Provides
    @IntoMap
    @DetailsCommands
    @StringKey(EditDetailsCommand.EDIT_DETAILS)
    public Command provideEditDetailsCommand(View view,
                                             @Proxy IAuthService authService,
                                             Provider<AccountComponent.Builder> provider) {
        return new EditDetailsCommand(view, authService, provider);
    }

    @Provides
    @IntoMap
    @DetailsCommands
    @StringKey(EXIT)
    public Command provideExitCommand(View view, Closeable closeable) {
        return new SystemExitCommand(view, closeable);
    }

    @Provides
    @IntoMap
    @DetailsCommands
    @StringKey(LOGOUT_SECTION)
    public Command provideGoToLoginSectionCommand(Provider<AuthComponent.Builder> provider) {
        return new LogoutCommand(provider);
    }

}

