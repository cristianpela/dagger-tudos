package home.crskdev.tododagger.account.details.di;

import javax.inject.Qualifier;

/**
 * Created by criskey on 22/3/2017.
 */
@Qualifier
public @interface DetailsCommands {
}
