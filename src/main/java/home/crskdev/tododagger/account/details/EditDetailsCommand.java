package home.crskdev.tododagger.account.details;

import home.crskdev.tododagger.account.di.AccountComponent;
import home.crskdev.tododagger.auth.model.AuthException;
import home.crskdev.tododagger.auth.model.IAuthService;
import home.crskdev.tododagger.core.command.Command;
import home.crskdev.tododagger.core.command.CommandException;
import home.crskdev.tododagger.core.view.View;

import javax.inject.Provider;
import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 * Created by criskey on 22/3/2017.
 */
public final class EditDetailsCommand implements Command {

    public static final String EDIT_DETAILS = "edit_details";

    //available to tests
    static final String DATE_FORMAT = "dd.MM.yyyy";

    //available to tests
    int step = 0;

    private UserDetails userDetails;

    private IAuthService authService;

    private Provider<AccountComponent.Builder> provider;

    private View view;

    private SimpleDateFormat dateFormat;

    public EditDetailsCommand(View view,
                              IAuthService authService,
                              Provider<AccountComponent.Builder> provider) {
        this.view = view;
        this.authService = authService;
        this.userDetails = authService.getUserDetails(authService
                .getPrincipal().name);
        this.provider = provider;
        dateFormat = new SimpleDateFormat(DATE_FORMAT);
        dateFormat.setLenient(false);
    }

    @Override
    public void execute(String... args) throws CommandException {
        String value = args.length > 0 ? args[0] : "";
        switch (step++) {
            case 0: {
                view.println("Enter your first name:");
                break;
            }
            case 1: {
                validateEmpty("First name", value);
                userDetails.firstName = value;
                view.println("Enter your last name:");
                break;
            }
            case 2: {
                validateEmpty("Last name", value);
                userDetails.lastName = value;
                view.println("Enter your birth date (" + DATE_FORMAT + "):");
                break;
            }
            case 3: {
                validateEmpty("Birth date", value);
                userDetails.birthdate = validateDate(value);
                view.println("Enter your details about you:");
                break;
            }
            case 4: {
                userDetails.about = value;
                save();
                break;
            }
        }
    }

    private void save() throws CommandException {
        try {
            authService.setUserDetails(userDetails);
            view.println("Details saved.");
            provider.get().build().init();
        } catch (AuthException e) {
            step--;
            throw new CommandException("Failed to save user details. Reason: " + e.getMessage() + " .Try again");
        }
    }

    private long validateDate(String value) throws CommandException {
        try {
            return dateFormat.parse(value).getTime();
        } catch (ParseException e) {
            step--;
            throw new CommandException("Invalid date format. Must be like: " + DATE_FORMAT + ". Ex: 22.06.1986 ");
        }
    }

    private void validateEmpty(String field, String value) throws CommandException {
        if (value.length() == 0) {
            step--;
            throw new CommandException(field + " is empty.");
        }
    }

    @Override
    public String name() {
        return EDIT_DETAILS;
    }

    @Override
    public boolean isDefault() {
        return true;
    }
}
