package home.crskdev.tododagger.account.details;

import home.crskdev.tododagger.core.command.Command;
import home.crskdev.tododagger.core.command.CommandException;
import home.crskdev.tododagger.core.command.CommandManager;
import home.crskdev.tododagger.core.command.ListenerSwapper;
import home.crskdev.tododagger.core.view.ANSI;
import home.crskdev.tododagger.core.view.Decorator;
import home.crskdev.tododagger.core.view.View;

import java.util.Map;

/**
 * Created by criskey on 22/3/2017.
 */
public class DetailsCommandManager extends CommandManager {

    public DetailsCommandManager(View view, Map<String, Command> commands, ListenerSwapper swapper) {
        super(view, commands, swapper);
    }

    @Override
    public void onReadLine(String readLine) {
        Command command = commands.get(EditDetailsCommand.EDIT_DETAILS);
        try {
            command.execute(readLine);
        } catch (CommandException e) {
            view.println(Decorator.builder(e.getMessage())
                    .foreground(ANSI.Color.RED)
                    .create());
        }
    }
}
