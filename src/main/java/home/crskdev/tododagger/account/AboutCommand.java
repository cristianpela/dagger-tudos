package home.crskdev.tododagger.account;

import home.crskdev.tododagger.account.details.UserDetails;
import home.crskdev.tododagger.auth.model.IAuthService;
import home.crskdev.tododagger.core.command.CommandException;
import home.crskdev.tododagger.core.command.ViewCommand;
import home.crskdev.tododagger.core.view.table.Table;
import home.crskdev.tododagger.core.view.View;

import java.text.SimpleDateFormat;

/**
 * Created by criskey on 21/3/2017.
 */
public class AboutCommand extends ViewCommand {

    public static final String ABOUT = "about";

    private IAuthService authService;

    //available to tests
    static final String DATE_FORMAT = "dd.MM.yyyy";

    private SimpleDateFormat dateFormat;


    public AboutCommand(View view, IAuthService authService) {
        super(view);
        this.authService = authService;
        dateFormat = new SimpleDateFormat(DATE_FORMAT);
    }

    @Override
    public void execute(String... args) throws CommandException {
        UserDetails userDetails = authService.getUserDetails(authService.getPrincipal().name);
        Table table = new Table(view, 20, 50);
        table.addRow("Username:", userDetails.username);
        table.addRow("First name:", userDetails.firstName);
        table.addRow("Last name:", userDetails.lastName);
        table.addRow("Birth date:", dateFormat.format(userDetails.birthdate));
        table.addRow("About me:", userDetails.about);
        table.display();
    }

    @Override
    public String name() {
        return ABOUT;
    }

    @Override
    public boolean isDefault() {
        return false;
    }
}
