package home.crskdev.tododagger.settings.di;

import dagger.Subcomponent;
import home.crskdev.tododagger.core.Init;
import home.crskdev.tododagger.core.InitComponent;

import javax.inject.Named;

/**
 * Created by criskey on 7/4/2017.
 */
@Subcomponent(modules = SettingsModule.class)
public interface SettingsComponent extends InitComponent {

    @Override
    @Named("settings-entry-point")
    Init init();

    @Subcomponent.Builder
    interface Builder extends InitComponent.Builder<SettingsComponent> {}
}
