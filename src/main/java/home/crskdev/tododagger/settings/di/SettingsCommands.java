package home.crskdev.tododagger.settings.di;

import javax.inject.Qualifier;
import java.lang.annotation.Retention;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Created by criskey on 7/4/2017.
 */
@Qualifier
@Retention(RUNTIME)
public @interface SettingsCommands {
}
