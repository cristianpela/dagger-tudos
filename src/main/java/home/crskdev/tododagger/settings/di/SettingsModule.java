package home.crskdev.tododagger.settings.di;

import dagger.Module;
import dagger.Provides;
import dagger.multibindings.IntoMap;
import dagger.multibindings.StringKey;
import home.crskdev.tododagger.auth.LogoutCommand;
import home.crskdev.tododagger.auth.di.AuthComponent;
import home.crskdev.tododagger.auth.di.Principal;
import home.crskdev.tododagger.core.Init;
import home.crskdev.tododagger.core.command.*;
import home.crskdev.tododagger.core.view.ANSI;
import home.crskdev.tododagger.core.view.View;
import home.crskdev.tododagger.hub.ExitToHubCommand;
import home.crskdev.tododagger.hub.di.HubComponent;
import home.crskdev.tododagger.settings.ANSIEnableCommand;

import javax.inject.Named;
import javax.inject.Provider;
import java.io.Closeable;
import java.util.Map;

import static home.crskdev.tododagger.auth.LogoutCommand.LOGOUT_SECTION;
import static home.crskdev.tododagger.core.command.SystemExitCommand.EXIT;
import static home.crskdev.tododagger.core.command.WelcomeCommand.WELCOME;
import static home.crskdev.tododagger.hub.ExitToHubCommand.HUB_SECTION;
import static home.crskdev.tododagger.settings.ANSIEnableCommand.ANSI_CMD;

/**
 * Created by criskey on 7/4/2017.
 */
@Module
public class SettingsModule {

    @Provides
    @Named("settings-entry-point")
    public Init provideSetingsEntryPoint(View view, @SettingsCommands Map<String, Command> commands,
                                  ListenerSwapper setter) {
        return new CommandManager(view, commands, setter);
    }

    //### Command Mapping####
    @Provides
    @IntoMap
    @SettingsCommands
    @StringKey(WELCOME)
    public static Command provideWelcomeCommand(View view, @Principal String principal) {
        return new WelcomeCommand(view, principal) {
            @Override
            public String[] availableCommands() {
                return new String[]{
                        ANSI_CMD,
                        HUB_SECTION,
                        LOGOUT_SECTION,
                        EXIT
                };
            }
        };
    }


    @Provides
    @IntoMap
    @SettingsCommands
    @StringKey(ANSI_CMD)
    public Command provideANSIEnableCommand(View view, ANSI ansi) {
        return new ANSIEnableCommand(view, ansi);
    }

    @Provides
    @IntoMap
    @SettingsCommands
    @StringKey(HUB_SECTION)
    public Command provideHubExitCommand(Provider<HubComponent.Builder> provider) {
        return new ExitToHubCommand(provider);
    }

    @Provides
    @IntoMap
    @SettingsCommands
    @StringKey(EXIT)
    public Command provideExitCommand(View view, Closeable closeable) {
        return new SystemExitCommand(view, closeable);
    }

    @Provides
    @IntoMap
    @SettingsCommands
    @StringKey(LOGOUT_SECTION)
    public Command provideGoToLoginSectionCommand(Provider<AuthComponent.Builder> provider) {
        return new LogoutCommand(provider);
    }

}
