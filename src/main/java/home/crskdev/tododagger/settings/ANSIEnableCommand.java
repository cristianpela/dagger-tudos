package home.crskdev.tododagger.settings;

import home.crskdev.tododagger.core.command.Command;
import home.crskdev.tododagger.core.command.CommandException;
import home.crskdev.tododagger.core.view.ANSI;
import home.crskdev.tododagger.core.view.View;

/**
 * Created by criskey on 7/4/2017.
 */
public class ANSIEnableCommand implements Command {

    public static final String ANSI_CMD = "ansi";

    private ANSI ansi;

    private View view;

    public ANSIEnableCommand(View view, ANSI ansi) {
        this.view = view;
        this.ansi = ansi;
    }

    @Override
    public void execute(String... args) throws CommandException {
        if (args.length != 2) {
            throw new CommandException("Bad input. You must use it like: ansi <on>|<off>");
        }
        String arg = args[1].trim().toLowerCase();
        if (arg.equals("on")) {
            ansi.enable = true;
            view.println("ANSI decorations enabled");
        } else if (arg.equals("off")) {
            ansi.enable = false;
            view.println("ANSI decorations disabled");
        } else {
            throw new CommandException("Unkown argument " + arg + ". You must use it like: ansi <on>|<off>");
        }
    }

    @Override
    public String name() {
        return ANSI_CMD;
    }

    @Override
    public boolean isDefault() {
        return false;
    }
}
