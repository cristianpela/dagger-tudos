package home.crskdev.tododagger.auth;

import home.crskdev.tododagger.auth.model.AuthException;
import home.crskdev.tododagger.auth.model.IAuthService;
import home.crskdev.tododagger.auth.model.User;
import home.crskdev.tododagger.core.command.Command;
import home.crskdev.tododagger.core.command.CommandException;
import home.crskdev.tododagger.core.command.ViewCommand;
import home.crskdev.tododagger.core.view.View;

/**
 * Created by criskey on 19/3/2017.
 */
public class RegisterCommand extends ViewCommand {

    public static final String REGISTER = "register";

    private final IAuthService authService;

    public RegisterCommand(View view, IAuthService authService) {
        super(view);
        this.authService = authService;
    }

    @Override
    public void execute(String... args) throws CommandException {
        if (args.length != 3) {
            throw new CommandException
                    ("Invalid arguments. Command should look like: register <username> <password>");
        }
        String username = args[1];
        char[] password = args[2].toCharArray();
        try {
            authService.register(new User(username, password));
            view.println("User " + username + " created");
        } catch (AuthException ex) {
            throw new CommandException(ex.getMessage());
        }
    }

    @Override
    public String name() {
        return REGISTER;
    }

    @Override
    public boolean isDefault() {
        return false;
    }
}
