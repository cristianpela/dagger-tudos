package home.crskdev.tododagger.auth;

import home.crskdev.tododagger.auth.di.AuthComponent;
import home.crskdev.tododagger.core.command.GoToSectionCommand;

import javax.inject.Provider;

/**
 * Created by criskey on 21/3/2017.
 */
public class LogoutCommand extends GoToSectionCommand<AuthComponent.Builder> {

    public static final String LOGOUT_SECTION = "logout";

    public LogoutCommand(Provider<AuthComponent.Builder> provider) {
        super(provider);
    }

    @Override
    public String name() {
        return LOGOUT_SECTION;
    }
}
