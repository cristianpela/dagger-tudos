package home.crskdev.tododagger.auth;

/**
 * Created by criskey on 31/3/2017.
 */
public interface IPasswordEncoder {

    String encode(char[] password, String salt);

    boolean matches(char[] password, String hash, String salt);
}
