package home.crskdev.tododagger.auth.di;

import com.rethinkdb.net.Connection;
import dagger.Module;
import dagger.Provides;
import dagger.multibindings.IntoMap;
import dagger.multibindings.StringKey;
import home.crskdev.tododagger.account.details.UserDetails;
import home.crskdev.tododagger.auth.AuthCommand;
import home.crskdev.tododagger.auth.BCryptPasswordEncoder;
import home.crskdev.tododagger.auth.IPasswordEncoder;
import home.crskdev.tododagger.auth.RegisterCommand;
import home.crskdev.tododagger.auth.model.*;
import home.crskdev.tododagger.core.Init;
import home.crskdev.tododagger.core.LazyCache;
import home.crskdev.tododagger.core.command.*;
import home.crskdev.tododagger.core.qualifiers.InMemory;
import home.crskdev.tododagger.core.qualifiers.Proxy;
import home.crskdev.tododagger.core.qualifiers.Rethink;
import home.crskdev.tododagger.core.view.ANSI;
import home.crskdev.tododagger.core.view.Decorator;
import home.crskdev.tododagger.core.view.View;
import home.crskdev.tododagger.hub.di.HubComponent;

import javax.inject.Named;
import javax.inject.Provider;
import java.io.Closeable;
import java.util.Map;
import java.util.Set;

import static home.crskdev.tododagger.auth.AuthCommand.AUTH;
import static home.crskdev.tododagger.auth.RegisterCommand.REGISTER;
import static home.crskdev.tododagger.core.command.SystemExitCommand.EXIT;

/**
 * Created by criskey on 18/3/2017.
 */
@Module(subcomponents = HubComponent.class)
public class AuthModule {

    @AuthScope
    @Provides
    @Named("bcrypt")
    public IPasswordEncoder provideBCryptEncoder() {
        return new BCryptPasswordEncoder();
    }

    @AuthScope
    @Provides
    @Proxy
    public IAuthService provideProxyAuthService(@Rethink IAuthService authService) {
        return new AuthServiceProxy(authService);
    }

    @AuthScope
    @Provides
    @InMemory
    public IAuthService provideMemoryAuthService(Set<User> users, Map<String, UserDetails> usersDetails) {
        return new InMemoryAuthService(users, usersDetails);
    }


    @AuthScope
    @Provides
    @Rethink
    public IAuthService provideRethinkAuthService(LazyCache<Connection> connection,
                                                  @Named("bcrypt") IPasswordEncoder passwordEncoder) {
        return new RethinkAuthService(connection, passwordEncoder);
    }

    @Provides
    @Principal
    public String providesPrincipal(@Proxy IAuthService authService) {
        return authService.getPrincipal().name;
    }

    @Provides
    @Named("auth-entry-point")
    public Init provideEntryPoint(View view, @AuthCommands Map<String, Command> commands, ListenerSwapper listenerSetter) {
        return new CommandManager(view, commands, listenerSetter);
    }

    //################Command Mapping################
    @Provides
    @IntoMap
    @AuthCommands
    @StringKey(AUTH)
    public Command provideAuthCommand(Provider<HubComponent.Builder> provider, @Proxy IAuthService authService) {
        return new AuthCommand(provider, authService);
    }

    @Provides
    @IntoMap
    @AuthCommands
    @StringKey(REGISTER)
    public Command provideRegisterCommand(View view, @Proxy IAuthService authService) {
        return new RegisterCommand(view, authService);
    }

    @Provides
    @IntoMap
    @AuthCommands
    @StringKey(EXIT)
    public Command provideExitCommand(View view, Closeable closeable) {
        return new SystemExitCommand(view, closeable);
    }

    @Provides
    @IntoMap
    @AuthCommands
    @StringKey("authprompt")
    public Command provideAuthPromptCommand(View view) {
        return new Command() {

            @Override
            public void execute(String... args) throws CommandException {
                StringBuilder logoBuilder = new StringBuilder();
                logoBuilder.append("  _____                                _______ ____  _____   ____   _____ \n");
                logoBuilder.append(" |  __ \\                              |__   __/ __ \\|  __ \\ / __ \\ / ____|\n");
                logoBuilder.append(" | |  | | __ _  __ _  __ _  ___ _ __     | | | |  | | |  | | |  | | (___  \n");
                logoBuilder.append(" | |  | |/ _` |/ _` |/ _` |/ _ \\ '__|    | | | |  | | |  | | |  | |\\___ \\ \n");
                logoBuilder.append(" | |__| | (_| | (_| | (_| |  __/ |       | | | |__| | |__| | |__| |____) |\n");
                logoBuilder.append(" |_____/ \\__,_|\\__, |\\__, |\\___|_|       |_|  \\____/|_____/ \\____/|_____/ \n");
                logoBuilder.append("                __/ | __/ |                                               \n");
                logoBuilder.append("              |___/ |___/                                                \n");
                view.println(Decorator.builder(logoBuilder.toString())
                        .foreground(ANSI.Color.BLUE)
                        .create());
                view.println("Enter your username and password. (Ex: auth <username> <password>)");
            }

            @Override
            public String name() {
                return "authprompt";
            }

            @Override
            public boolean isDefault() {
                return true;
            }
        };
    }


}
