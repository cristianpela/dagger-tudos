package home.crskdev.tododagger.auth.di;

import javax.inject.Qualifier;
import java.lang.annotation.Retention;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Created by criskey on 19/3/2017.
 */
@Qualifier
@Retention(RUNTIME)
public @interface Principal {
}
