package home.crskdev.tododagger.auth.di;

import javax.inject.Scope;

/**
 * Created by criskey on 20/3/2017.
 */
@Scope
public @interface AuthScope {
}
