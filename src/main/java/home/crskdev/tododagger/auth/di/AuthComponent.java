package home.crskdev.tododagger.auth.di;

import dagger.Subcomponent;
import home.crskdev.tododagger.core.Init;
import home.crskdev.tododagger.core.InitComponent;

import javax.inject.Named;

/**
 * Created by criskey on 20/3/2017.
 */
@Subcomponent(modules = {AuthModule.class})
@AuthScope
public interface AuthComponent extends InitComponent {

    @Override
    @Named("auth-entry-point")
    Init init();

    @Subcomponent.Builder
    interface Builder extends InitComponent.Builder<AuthComponent> {
        AuthComponent build();
    }
}
