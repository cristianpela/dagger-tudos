package home.crskdev.tododagger.auth;

import dagger.Provides;
import home.crskdev.tododagger.auth.di.AuthScope;
import org.mindrot.jbcrypt.BCrypt;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * Created by criskey on 31/3/2017.
 */
public class BCryptPasswordEncoder implements IPasswordEncoder {

    @Inject
    public BCryptPasswordEncoder() {
    }

    public String encode(char[] password, String salt) {
        if (salt != null) {
            throw new IllegalArgumentException("Salt must be null. Let BCrypt handle salting");
        }
        return BCrypt.hashpw(new String(password), BCrypt.gensalt());
    }

    @Override
    public boolean matches(char[] password, String hash, String salt) {
        if (salt != null) {
            throw new IllegalArgumentException("Salt must be null. In BCrypt salt is encoded in hash");
        }
        return BCrypt.checkpw(new String(password), hash);
    }

}
