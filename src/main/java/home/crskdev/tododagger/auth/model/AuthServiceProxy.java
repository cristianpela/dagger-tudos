package home.crskdev.tododagger.auth.model;

import home.crskdev.tododagger.account.details.UserDetails;

import java.util.Arrays;

/**
 * Created by criskey on 31/3/2017.
 */
public class AuthServiceProxy implements IAuthService {

    protected User loggedUser;

    private IAuthService delegate;

    public AuthServiceProxy(IAuthService delegate) {
        this.delegate = delegate;
    }

    @Override
    public void login(User user) throws AuthException {
        try {
            delegate.login(user);
            loggedUser = user;
            Arrays.fill(loggedUser.password, ' ');
        } catch (AuthException e) {
            throw e;
        }
    }

    @Override
    public void logout() throws AuthException {
        loggedUser = null;
    }

    @Override
    public void register(User user) throws AuthException {
        if (isLoggedIn())
            throw new AuthException("Log out first before register.");
        try {
            delegate.register(user);
        } catch (AuthException e) {
            throw e;
        }
    }

    @Override
    public void changePassword(User principal, char[] newpass) throws AuthException {
        //if not logged in or is not principal then return false;
        if (!isLoggedIn()) {
            throw new AuthException("In order to change your password you must be logged in");
        }
        if (!principal.name.equals(loggedUser.name)) {
            throw new AuthException("You're not allowed to change others user password");
        }
        //if old password is wrong then return false;
        try {
            delegate.login(principal);
        } catch (AuthException e) {
            throw new AuthException("Your old password is incorrect.");
        }

        try {
            delegate.changePassword(principal, newpass);
            //refresh logged user;
            loggedUser = new User(principal.name, newpass);
        } catch (AuthException e) {
            throw e;
        }
    }

    @Override
    public void setUserDetails(UserDetails userDetails) throws AuthException {
        if (!isLoggedIn()) {
            throw new AuthException("In order to change your details, you must be logged in");
        }
        if (!userDetails.username.equals(loggedUser.name)) {
            throw new AuthException("You can not change other user's details");
        }
        try {
            delegate.setUserDetails(userDetails);
        } catch (AuthException e) {
            throw e;
        }
    }

    @Override
    public User getPrincipal() {
        return (isLoggedIn()) ? loggedUser : null;
    }

    @Override
    public UserDetails getUserDetails(String username) throws AuthException {
        if (!isLoggedIn()) {
            throw new AuthException("You're not logged in");
        }
//        if(!username.equals(loggedUser.name)){
//            throw new AuthException("You can't see other user's details");
//        }
        try {
            return delegate.getUserDetails(username);
        } catch (AuthException e) {
            throw e;
        }
    }

    private boolean isLoggedIn() {
        return loggedUser != null;
    }
}
