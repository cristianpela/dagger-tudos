package home.crskdev.tododagger.auth.model;

import java.util.Arrays;

/**
 * Created by criskey on 18/3/2017.
 */
public final class User implements Comparable<User> {

    public final String name;

    public final char[] password;

    public User(String name, char[] password) {
        this.name = name;
        this.password = password;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        User user = (User) o;

        if (!name.equals(user.name)) return false;
        return Arrays.equals(password, user.password);

    }

    @Override
    public int hashCode() {
        int result = name.hashCode();
        result = 31 * result + Arrays.hashCode(password);
        return result;
    }

    @Override
    public int compareTo(User o) {
        return this.name.compareTo(o.name);
    }
}
