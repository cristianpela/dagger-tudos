package home.crskdev.tododagger.auth.model;

import home.crskdev.tododagger.account.details.UserDetails;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

/**
 * Created by criskey on 18/3/2017.
 */
public class InMemoryAuthService implements IAuthService {

    private TreeSet<User> users;

    private Map<String, UserDetails> usersDetails;

    public InMemoryAuthService(Set<User> users, Map<String, UserDetails> usersDetails) {
        //NOTE: enforce using a TreeSet in order to respect the add "compare to"-based contract
        //see register method.
        //If a HashSet is used, this will allow to register users with same username
        //but different passwords, and thus bugging out the register
        this.users = new TreeSet<>(users);
        this.usersDetails = usersDetails;
    }

    public InMemoryAuthService() {
        this(new TreeSet<>(), new HashMap<>());
    }

    public InMemoryAuthService(Set<User> users) {
        this(users, new HashMap<>());
    }

    @Override
    public void login(User tryUser) {
        //TreeSet#contains doesn't follow equals contract but compareTo, so I'm using my own "contains"
        boolean logged = users.stream().anyMatch(user -> user.equals(tryUser));
        if (!logged) {
            throw new AuthException();
        }
    }

    @Override
    public void logout() {
        throw new UnsupportedOperationException();
    }

    @Override
    public void register(User user) {
        //add follows the compare contract from Comparator.(even though javadoc states otherwise)
        //reason: TreeSet is based on a NavigableTreeMap under the hood.
        //So, in User#compareTo, only usernames are compared.
        boolean registered = users.add(user);
        if (!registered)
            throw new AuthException("The username " + user.name + " is already taken");
    }

    @Override
    public void changePassword(User principal, char[] newpass) {
        users.remove(new User(principal.name, null));
        users.add(new User(principal.name, newpass));
    }

    @Override
    public void setUserDetails(UserDetails userDetails) throws AuthException {
        usersDetails.put(userDetails.username, userDetails);
    }

    @Override
    public User getPrincipal() {
        throw new UnsupportedOperationException();
    }

    @Override
    public UserDetails getUserDetails(String username) throws AuthException {
        UserDetails userDetails = usersDetails.get(username);
        return (userDetails == null) ? UserDetails.empty(username) : userDetails;
    }

}
