package home.crskdev.tododagger.auth.model;

import com.rethinkdb.gen.ast.Db;
import com.rethinkdb.net.Connection;
import home.crskdev.tododagger.account.details.UserDetails;
import home.crskdev.tododagger.auth.IPasswordEncoder;
import home.crskdev.tododagger.core.LazyCache;

import java.util.HashMap;

import static com.rethinkdb.RethinkDB.r;

/**
 * Created by criskey on 31/3/2017.
 */
public class RethinkAuthService implements IAuthService {

    private static final String TABLE_USER = "user";

    private static final String PK_NAME = "name";
    private static final String PASSWORD = "password";

    private static final String TABLE_USER_DETAILS = "user_details";

    private static final String FIRST_NAME = "first_name";
    private static final String LAST_NAME = "last_name";
    private static final String BIRTH_DATE = "birth_date";
    private static final String ABOUT = "about";

    private LazyCache<Connection> lazyConn;

    private IPasswordEncoder passwordEncoder;

    public RethinkAuthService(LazyCache<Connection> lazyConn, IPasswordEncoder passwordEncoder) {
        this.lazyConn = lazyConn;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public void login(User user) throws AuthException {
        try {
            HashMap result = db().table(TABLE_USER).get(user.name).run(getConn());
            if (result == null ||
                    !passwordEncoder.matches(user.password, (String) result.get(PASSWORD), null))
                throw new AuthException("Invalid credentials");
        } catch (Exception e) {
            throw new AuthException(e.getMessage());
        }
    }

    @Override
    public void logout() throws AuthException {
        throw new UnsupportedOperationException();
    }

    @Override
    public void register(User user) throws AuthException {
        try {
            HashMap result = db().table(TABLE_USER)
                    .insert(r.hashMap(PK_NAME, user.name)
                            .with(PASSWORD, passwordEncoder.encode(user.password, null)))
                    .run(getConn());
            if (Integer.parseInt(result.get("errors").toString()) > 0) {
                throw new AuthException("Username " + user.name + " already taken, try other one!");
            }
        } catch (Exception e) {
            throw new AuthException(e.getMessage());
        }
    }


    @Override
    public void changePassword(User principal, char[] newpass) throws AuthException {
        try {
            db().table(TABLE_USER).get(principal.name)
                    .update(r.hashMap(PASSWORD, passwordEncoder.encode(newpass, null)))
                    .run(getConn());
        } catch (Exception e) {
            throw new AuthException(e.getMessage());
        }
    }

    @Override
    public void setUserDetails(UserDetails userDetails) throws AuthException {
        try {
            db().table(TABLE_USER_DETAILS)
                    .insert(r.hashMap(PK_NAME, userDetails.username)
                            .with(FIRST_NAME, userDetails.firstName)
                            .with(LAST_NAME, userDetails.lastName)
                            .with(BIRTH_DATE, userDetails.birthdate)
                            .with(ABOUT, userDetails.about))
                    .optArg("conflict", "update")
                    .run(getConn());
        } catch (Exception e) {
            throw new AuthException(e.getMessage());
        }
    }

    @Override
    public User getPrincipal() {
        throw new UnsupportedOperationException();
    }

    @Override
    public UserDetails getUserDetails(String username) throws AuthException {
        HashMap<String, ?> result;
        try {
            result = db().table(TABLE_USER_DETAILS).get(username)
                    .run(getConn());
        } catch (Exception e) {
            throw new AuthException(e.getMessage());
        }
        UserDetails constructed;
        if (result != null) {
            constructed = new UserDetails.Builder(username)
                    .setFirstName((String) result.get(FIRST_NAME))
                    .setLastName((String) result.get(LAST_NAME))
                    .setBirthdate((Long) result.get(BIRTH_DATE))
                    .setAbout((String) result.get(ABOUT))
                    .create();
        } else {
            constructed = UserDetails.empty(username);
        }
        return constructed;
    }


    private Db db() {
        return r.db("test");
    }

    private Connection getConn() throws AuthException {
        return lazyConn.get();
    }

}
