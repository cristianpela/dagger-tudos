package home.crskdev.tododagger.auth.model;

import home.crskdev.tododagger.account.details.UserDetails;

/**
 * Created by criskey on 18/3/2017.
 */
public interface IAuthService {

    void login(User user) throws AuthException;

    void logout() throws AuthException;

    void register(User user) throws AuthException;

    void changePassword(User principal, char[] newpass) throws AuthException;

    void setUserDetails(UserDetails userDetails) throws AuthException;

    User getPrincipal();

    UserDetails getUserDetails(String username) throws AuthException;

}
