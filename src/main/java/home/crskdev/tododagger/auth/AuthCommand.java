package home.crskdev.tododagger.auth;

import home.crskdev.tododagger.auth.model.AuthException;
import home.crskdev.tododagger.auth.model.IAuthService;
import home.crskdev.tododagger.auth.model.User;
import home.crskdev.tododagger.core.command.Command;
import home.crskdev.tododagger.core.command.CommandException;
import home.crskdev.tododagger.hub.di.HubComponent;

import javax.inject.Provider;

/**
 * Created by criskey on 19/3/2017.
 */
public class AuthCommand implements Command {

    public static final String AUTH = "auth";

    private Provider<HubComponent.Builder> provider;

    private final IAuthService authService;

    public AuthCommand(Provider<HubComponent.Builder> provider, IAuthService authService) {
        this.provider = provider;
        this.authService = authService;
    }

    @Override
    public void execute(String... args) throws CommandException {
        if (args.length != 3) {
            throw new CommandException("Invalid command arguments. Must be like: auth <username> <password>");
        }
        try {
            authService.login(new User(args[1], args[2].toCharArray()));
            //launch HubComponent
            provider.get().build().init();
        } catch (AuthException ex) {
            throw new CommandException(ex.getMessage());
        }
    }

    @Override
    public String name() {
        return AUTH;
    }

    @Override
    public boolean isDefault() {
        return false;
    }

}
