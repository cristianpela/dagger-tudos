package home.crskdev.tododagger.core.qualifiers;

import javax.inject.Qualifier;

/**
 * Created by criskey on 3/4/2017.
 */
@Qualifier
public @interface Rethink {
}
