package home.crskdev.tododagger.core.view.table;

import java.util.List;

/**
 * Created by criskey on 5/4/2017.
 */
public interface CellRenderer {

    RenderedCell render(int row, int col, Cell cell);

}
