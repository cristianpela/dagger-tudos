package home.crskdev.tododagger.core.view;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.regex.Pattern;

/**
 * Created by criskey on 7/4/2017.
 */
@Singleton
public class ANSI {

    private static final String PREFIX = "\033";

    private static final Pattern PATTERN = Pattern.compile("\\" + PREFIX + "\\[[a-z0-9]*m");

    public boolean enable;

    @Inject
    public ANSI() {
    }

    public String inspect(String text) {
        if (!enable) {
            return PATTERN.matcher(text).replaceAll("");
        }
        return text;
    }

    public interface Format {
        String BOLD = "\u001b[1m";
        String ITALIC = "\u001b[1m";
        String UNDERLINED = "\u001b[4m";
        String BLINK = "\u001b[5m";
    }

    public enum Color {
        RESET(Color.buildCode(-1), -1),
        BLACK(Color.buildCode(-1), -1),
        RED(Color.buildCode(1), 1),
        GREEN(Color.buildCode(2), 2),
        YELLOW(Color.buildCode(3), 3),
        BLUE(Color.buildCode(4), 4),
        MAGENTA(Color.buildCode(5), 5),
        CYAN(Color.buildCode(6), 6),
        WHITE(Color.buildCode(7), 7);

        private String code;
        private int id;

        Color(String code, int id) {
            this.code = code;
            this.id = id;
        }

        static String buildCode(int code) {
            if (code == -1) {
                return PREFIX + "[0m";
            }

            if (code < 1 || code > 7) {
                throw new IllegalArgumentException("Code must be between 1 and 7");
            }
            return PREFIX + "[3" + code + "m";
        }

        public static String toBackground(Color Color) {
            if (Color.id != -1) {
                return PREFIX + "[4" + Color.id + "m";
            } else {
                return PREFIX + "[40m";
            }
        }


        @Override
        public String toString() {
            return code;
        }

    }
}
