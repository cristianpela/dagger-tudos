package home.crskdev.tododagger.core.view.table;

import home.crskdev.tododagger.core.view.ANSI;

/**
 * Created by criskey on 31/3/2017.
 */
abstract class Divider {

    public static final ANSI.Color RESET_ANSI_SYMBOL = ANSI.Color.BLACK;

    protected static final char SPACE = ' ';

    protected final int padding;

    protected final char pattern;

    protected final ANSI.Color Color;

    public Divider(int padding, char pattern, ANSI.Color Color) {
        this.pattern = pattern;
        this.padding = padding;
        this.Color = Color;
    }

    public Divider(int padding, char pattern) {
        this(padding, pattern, RESET_ANSI_SYMBOL);
    }

    abstract String generate();

}
