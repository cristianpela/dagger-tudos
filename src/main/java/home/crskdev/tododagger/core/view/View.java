package home.crskdev.tododagger.core.view;

/**
 * Created by criskey on 23/3/2017.
 */
public interface View {


    void print(String text);

    void println(String text);

    void format(String text, Object... args);

}
