package home.crskdev.tododagger.core.view.table;

import home.crskdev.tododagger.core.view.table.schema.Schema;

/**
 * Created by criskey on 31/3/2017.
 */
public final class Cell {

    public final boolean isHeader;

    public final Object content;

    public final Schema<?> schema;

    Cell(boolean isHeader, Object content, Schema<?> schema) {
        this.isHeader = isHeader;
        if (!content.getClass().equals(schema.getType())) {
            throw new IllegalStateException("Schema type " + schema.getType().getSimpleName()
                    + " doesn't match content type " + content.getClass().getSimpleName());
        }
        this.content = content;
        this.schema = schema;
    }

    Cell(Object content, Schema<?> schema) {
        this(false, content, schema);
    }
}
