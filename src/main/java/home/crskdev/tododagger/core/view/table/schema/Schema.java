package home.crskdev.tododagger.core.view.table.schema;

/**
 * Created by criskey on 4/4/2017.
 */
public class Schema<T> {

    public final int width;

    private Class<T> type;

    public Schema(int width, Class<T> type) {
        this.width = width;
        this.type = type;
    }

    public Class<T> getType() {
        return type;
    }
}
