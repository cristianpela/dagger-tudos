package home.crskdev.tododagger.core.view.table.schema;

import home.crskdev.tododagger.core.view.table.Alignment;

/**
 * Created by criskey on 5/4/2017.
 */
public class StringSchema extends Schema<String> {

    public StringSchema(int width) {
        super(width, String.class);
    }

}
