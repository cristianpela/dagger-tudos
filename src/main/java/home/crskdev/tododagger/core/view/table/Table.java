package home.crskdev.tododagger.core.view.table;

import home.crskdev.tododagger.core.view.Decorator;
import home.crskdev.tododagger.core.view.TextUtils;
import home.crskdev.tododagger.core.view.View;
import home.crskdev.tododagger.core.view.table.schema.*;

import java.text.SimpleDateFormat;
import java.util.*;

import static home.crskdev.tododagger.core.view.ANSI.Color.BLUE;
import static home.crskdev.tododagger.core.view.ANSI.Color.WHITE;
import static home.crskdev.tododagger.core.view.table.Alignment.Type;
import static home.crskdev.tododagger.core.view.table.Alignment.Type.*;
import static home.crskdev.tododagger.core.view.table.VerticalDivider.Place.*;

/**
 * Created by criskey on 24/3/2017.
 */
public final class Table {

    protected static final int DEFAULT_WIDTH = 30;

    protected static final int DEFAULT_PADDING = 0;

    private static final DividersSpec DEFAULT_DIVIDER_PATTERN = new DividersSpec('-', '|');

    private View view;

    private List<Row> rows;

    private HorizontalDivider hDivider;

    private VerticalDivider vDivider;

    private Schema[] schemas;

    private List<CellRenderer> cellRenderers;

    private List<CellDecorator> cellDecorators;

    private SimpleDateFormat simpleDateFormat;

    private CellRenderer defaultCellRenderer = (row, col, cell) -> {

        Schema<?> schema = cell.schema;
        int w = schema.width;

        if (schema instanceof BooleanSchema) {
            Boolean content = (Boolean) cell.content;
            return new RenderedCell(w, CENTER, (content) ? "[x]" : "[]");
        } else if (schema instanceof IntSchema) {
            Integer content = (Integer) cell.content;
            return new RenderedCell(w, RIGHT, Integer.toString(content));
        } else if (schema instanceof FloatSchema) {
            Float content = (Float) cell.content;
            String format = String.format("%." + ((FloatSchema) schema).decimals + "f", content);
            return new RenderedCell(w, RIGHT, format);
        } else if (schema instanceof DateSchema) {
            Date date = (Date) cell.content;
            if (simpleDateFormat == null)
                simpleDateFormat = new SimpleDateFormat(((DateSchema) schema).format);
            return new RenderedCell(w, Type.CENTER, simpleDateFormat.format(date));
        }

        String content = cell.content.toString();
        Type alignment = cell.isHeader ? CENTER : LEFT;
        return new RenderedCell(schema.width, alignment, content);

    };

    private CellDecorator defaultCellDecorator = (row, col, decorator, cell) -> {
        if (cell.isHeader) {
            decorator.bold()
                    .foreground(WHITE)
                    .background(BLUE)
                    .create();
        }
        return decorator.create();
    };


    public Table(View view, DividersSpec dividersSpec, Schema... schemas) {
        if (schemas.length == 0) {
            throw new IllegalArgumentException("Schema outputs are empty.");
        }
        this.view = view;
        this.schemas = schemas;
        rows = new ArrayList<>();

        hDivider = new HorizontalDivider(getWidth(), schemas.length, DEFAULT_PADDING,
                dividersSpec.horizontal, dividersSpec.hColor);
        vDivider = new VerticalDivider(DEFAULT_PADDING, dividersSpec.vertical, dividersSpec.vColor);

        cellRenderers = new LinkedList<>();
        cellRenderers.add(defaultCellRenderer);
        cellDecorators = new LinkedList<>();
        cellDecorators.add(defaultCellDecorator);
    }

    public Table(View view, Schema... schemas) {
        this(view, DEFAULT_DIVIDER_PATTERN, schemas);
    }

    public Table(View view, DividersSpec dividersSpec, int... colWidths) {
        this(view, dividersSpec, constructDefaultOutputWidths(colWidths));
    }

    public Table(View view, int... colWidths) {
        this(view, DEFAULT_DIVIDER_PATTERN, colWidths);
    }

    public Table(View view, DividersSpec dividersSpec, int noCols) {
        this(view, dividersSpec, constructDefaultOutputWidths(
                constructDefaultColumnWidths(noCols)));
    }

    public Table(View view, int noCols) {
        this(view, DEFAULT_DIVIDER_PATTERN, noCols);
    }

    private static int[] constructDefaultColumnWidths(int noCols) {
        int[] colsWidth = new int[Math.abs(noCols)];
        Arrays.fill(colsWidth, DEFAULT_WIDTH);
        return colsWidth;
    }

    private static Schema[] constructDefaultOutputWidths(int[] widths) {
        Schema[] schemas = new Schema[widths.length];
        for (int i = 0; i < widths.length; i++) {
            schemas[i] = new StringSchema(widths[i]);
        }
        return schemas;
    }

    public void addCellRenderer(CellRenderer cellRenderer) {
        cellRenderers.add(cellRenderer);
    }

    public void addCellDecorator(CellDecorator cellDecorator) {
        cellDecorators.add(cellDecorator);
    }

    private List<String> render(int row, int col, Cell cell) {
        ListIterator<CellRenderer> li = cellRenderers.listIterator(cellRenderers.size());
        RenderedCell rendered = null;
        while (li.hasPrevious()) {
            rendered = li.previous().render(row, col, cell);
            if (rendered != null) // when get the first match we return;
                break;
        }
        if (rendered == null) {
            //should not reach this
            throw new IllegalStateException("Render not present in cell " + cell.toString());
        }
        return rendered.getContent();
    }

    private String decorate(int row, int col, String renderedContent, Cell cellValue) {
        ListIterator<CellDecorator> li = cellDecorators.listIterator(cellDecorators.size());
        String decorated = null;
        while (li.hasPrevious()) {
            decorated = li.previous().decorate(row, col, Decorator.builder(renderedContent), cellValue);
            if (decorated != null) {
                return decorated;
            }
        }
        if (decorated == null) {
            //should not reach this
            throw new IllegalStateException("Decorator not present in cell (r:" + row + " c:" + col + ")");
        }
        return decorated;
    }

    private int getWidth() {
        int width = 0;
        for (int i = 0; i < schemas.length; i++) {
            width += schemas[i].width;
        }
        return width;
    }

    private void addRow(boolean isHeader, Object... contents) {
        if (contents.length != schemas.length) {
            throw new IllegalStateException("Content length must match schemas length : " + schemas.length);
        }
        Cell[] cells = new Cell[schemas.length];
        for (int i = 0; i < schemas.length; i++) {
            Schema schema = (isHeader) ? new StringSchema(schemas[i].width) : schemas[i];
            cells[i] = new Cell(isHeader, contents[i], schema);
        }
        rows.add(new Row(isHeader, cells));
    }

    public void addRow(Object... contents) {
        addRow(false, contents);
    }

    public void addHeaderRow(Object... contents) {
        addRow(true, contents);
    }

    private int maxBucketDepth(List<List<String>> wraps) {
        int max = 0;
        for (List<String> wrap : wraps) {
            if (wrap.size() > max) {
                max = wrap.size();
            }
        }
        return max;
    }


    //for testing
    protected Collection<Row> getRows() {
        return Collections.unmodifiableCollection(rows);
    }

    public void display() {
        if (rows.isEmpty()) {
            return; // nothing to display
        }
        view.println(hDivider.generate());
        for (int i = 0; i < rows.size(); i++) {
            displayRow(i, rows.get(i));
        }
    }

    /**
     * Add row to a table. If column content is bigger than the column width then
     * the content/content will be split in chunks no bigger then the column with.
     * This result in more rows<br/>
     * For example we have 1, 5, 1 column dimensions<br>
     * We add ['a', 'aaaaabbbbb', 'c']. The table will add one more row and result will be:
     * <br/>
     * ['a', 'aaaaa', 'c']<br/>
     * ['', 'bbbbb', '']
     */
    private void displayRow(int rowIndex, Row row) {
        List<List<String>> buckets = new ArrayList<>();
        //we split each column content in a bucket of chunks(list o chunks) no bigger than the column size,
        //and then add each column's bucket in a list
        List<Cell> cells = row.getCells();
        for (int i = 0; i < schemas.length; i++) {
            buckets.add(render(rowIndex, i, cells.get(i)));
        }
        //next we find the deepest bucket (list with biggest size) in the method #maxBucketDepth
        //then we take a chunk from i - index of each bucket and add it to a column content array.
        // if i - index is bigger than current bucket size a placeholder (''-character) is added to columnContents
        for (int i = 0, max = maxBucketDepth(buckets); i < max; i++) {
            List<String> rowContents = new ArrayList<>();
            vDivider.setPlace(START);
            rowContents.add(vDivider.generate());
            vDivider.setPlace(MIDDLE);

            for (int col = 0; col < buckets.size(); col++) {
                List<String> bucket = buckets.get(col);
                if (i == cells.size() - 1) {
                    vDivider.setPlace(END);
                }
                String renderedContent = (i < bucket.size())
                        ? bucket.get(i)
                        : TextUtils.repeat(schemas[col].width, ' ');
                rowContents.add(decorate(rowIndex, col, renderedContent, cells.get(col)));
                rowContents.add(vDivider.generate());
            }

            StringBuilder pattern = new StringBuilder();
            for (int j = 0; j < schemas.length; j++) {
                pattern.append("%" + schemas[j].width + "s%s");
            }

            vAlign(rowContents);

            Object[] formatArgs = rowContents.toArray(new String[rowContents.size()]);
            view.format("%s" + pattern.toString() + "\n", formatArgs);

            if (i == max - 1) { // if is not inner row;
                view.println(hDivider.generate());
            }
        }
    }

    private void vAlign(List<String> rowContents) {
        //TODO implement vAlign
    }

}
