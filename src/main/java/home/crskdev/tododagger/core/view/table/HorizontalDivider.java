package home.crskdev.tododagger.core.view.table;

import home.crskdev.tododagger.core.view.ANSI;
import home.crskdev.tododagger.core.view.Decorator;
import home.crskdev.tododagger.core.view.TextUtils;

/**
 * Created by criskey on 31/3/2017.
 */
class HorizontalDivider extends Divider {

    final int width;


    public HorizontalDivider(int tableWidth, int noCols, int padding, char pattern, ANSI.Color Color) {
        super(padding, pattern, Color);
        checkArguments(tableWidth, noCols, padding);
        width = calculateWidth(tableWidth, noCols, padding);
    }


    public HorizontalDivider(int tableWidth, int noCols, int padding, char pattern) {
        this(tableWidth, noCols, padding, pattern, RESET_ANSI_SYMBOL);
    }

    public HorizontalDivider(int tableWidth, int noCols, int padding, ANSI.Color Color) {
        this(tableWidth, noCols, padding, '-', Color);
    }

    public HorizontalDivider(int tableWidth, int noCols, int padding) {
        this(tableWidth, noCols, padding, RESET_ANSI_SYMBOL);
    }

    private void checkArguments(int tableWidth, int noCols, int padding) {
        if (tableWidth < 0 || noCols < 0 || padding < 0)
            throw new IllegalArgumentException("Arguments must be positive");
        if (tableWidth < padding)
            throw new IllegalArgumentException("Padding should be lesser than table width");
    }

    @Override
    String generate() {
        return Decorator.builder(TextUtils.repeat(width, pattern))
                .foreground(Color)
                .bold()
                .create();
    }

    private int calculateWidth(int tableWidth, int noCols, int padding) {
        return tableWidth + noCols * 2 * padding + noCols + 1;
    }

}
