package home.crskdev.tododagger.core.view;

import javax.inject.Inject;

/**
 * Created by criskey on 23/3/2017.
 */
public class SystemView implements View {

    private ANSI ansi;

    @Inject
    public SystemView(ANSI ansi) {
        this.ansi = ansi;
    }

    @Override
    public void print(String text) {
        System.out.print(ansi.inspect(text));
    }

    @Override
    public void println(String text) {
        System.out.println(ansi.inspect(text));
    }

    @Override
    public void format(String template, Object... args) {
        for (int i = 0; i < args.length; i++) {
            if(args[i] instanceof String){
                args[i] = ansi.inspect((String) args[i]);
            }
        }
        System.out.format(template, args);
    }

}
