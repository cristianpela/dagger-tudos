package home.crskdev.tododagger.core.view.table;

/**
 * Created by criskey on 4/4/2017.
 */
public class Alignment {

    private final Type type;

    private final int width;

    public Alignment(Type type, int width) {
        this.type = type;
        this.width = width;
    }

    public Alignment(int width) {
        this(Type.NONE, width);
    }

    private void checkArgs(String text, int width) {
        if (text.length() > width) {
            throw new IllegalArgumentException("Text length " + text.length() + " must be at most with " + width);
        }
        if (width <= 0) {
            throw new IllegalArgumentException("Width must pe positive");
        }
    }

    public String align(String text) {
        checkArgs(text, width);
        switch (type) {
            case LEFT:
                return String.format("%-" + width + "s", text);
            case RIGHT:
                return spaces(width - text.length()) + text;
            case CENTER:
                return center(text);
            default:
                return text;

        }
    }

    public enum Type {
        NONE, LEFT, CENTER, RIGHT
    }

    private String spaces(int width) {
        return String.format("%" + width + "s", "");
    }

    private String center(String text) {
        String space = " ";
        StringBuilder builder = new StringBuilder(text);
        int step = 1;
        while (builder.length() < width) {
            if (++step % 2 == 0) {
                builder.insert(0, space);
            } else {
                builder.append(space);
            }
        }
        return builder.toString();
    }

}
