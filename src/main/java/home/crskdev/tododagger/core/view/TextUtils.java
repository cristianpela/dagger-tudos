package home.crskdev.tododagger.core.view;

import home.crskdev.tododagger.core.view.table.ContentWrapper;

import java.util.List;

/**
 * Created by criskey on 6/4/2017.
 */
public final class TextUtils {

    public static String repeat(int length, char patternType) {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < length; i++) {
            builder.append(patternType);
        }
        return builder.toString();
    }

    public static List<String> wrap(int width, String text){
        return ContentWrapper.INSTANCE.wrap(width, text);
    }
}
