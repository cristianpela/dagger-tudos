package home.crskdev.tododagger.core.view.table.schema;

/**
 * Created by criskey on 5/4/2017.
 */
public class IntSchema extends Schema<Integer> {
    public IntSchema(int width) {
        super(width, Integer.class);
    }
}
