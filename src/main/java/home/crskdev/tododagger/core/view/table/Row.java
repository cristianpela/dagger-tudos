package home.crskdev.tododagger.core.view.table;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by criskey on 31/3/2017.
 */
final class Row {

    final boolean isHeader;

    private List<Cell> cellList;

    Row(boolean isHeader, Cell... cells) {
        this.isHeader = isHeader;
        cellList = new ArrayList<>();
        for (int i = 0; i < cells.length; i++) {
            cellList.add(cells[i]);
        }
    }

//    private Row(int[] colWidths, String... columnsText) {
////        this(false, false, colWidths, columnsText);
//    }

    protected List<Cell> getCells() {
        return Collections.unmodifiableList(cellList);
    }


}
