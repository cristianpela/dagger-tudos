package home.crskdev.tododagger.core.view.table.schema;

import java.util.Date;

/**
 * Created by criskey on 5/4/2017.
 */
public class DateSchema extends Schema<Date> {

    public static final String DEFAULT_FORMAT = "dd-MM-yyy hh:mm:ss";

    public final String format;

    public DateSchema(int width, String format) {
        super(width, Date.class);
        this.format = format;
    }

    public DateSchema(int width) {
        this(width, DEFAULT_FORMAT);
    }
}
