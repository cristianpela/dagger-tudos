package home.crskdev.tododagger.core.view.table.schema;

/**
 * Created by criskey on 5/4/2017.
 */
public class BooleanSchema extends Schema<Boolean> {

    public BooleanSchema(int width) {
        super(width, Boolean.class);
    }

}
