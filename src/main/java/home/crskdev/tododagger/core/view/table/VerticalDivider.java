package home.crskdev.tododagger.core.view.table;

import home.crskdev.tododagger.core.view.ANSI;
import home.crskdev.tododagger.core.view.TextUtils;

import static home.crskdev.tododagger.core.view.TextUtils.repeat;

/**
 * Created by criskey on 31/3/2017.
 */
class VerticalDivider extends Divider {

    enum Place {
        START, MIDDLE, END;
    }

    private Place place;

    public VerticalDivider(int padding, char pattern, ANSI.Color Color) {
        super(padding, pattern, Color);
        place = Place.START;
    }

    public VerticalDivider(int padding, char pattern) {
        super(padding, pattern, RESET_ANSI_SYMBOL);
        place = Place.START;
    }

    public VerticalDivider(int padding) {
        this(padding, '|', RESET_ANSI_SYMBOL);
    }

    void setPlace(Place place) {
        this.place = place;
    }

    @Override
    String generate() {
        StringBuilder builder = new StringBuilder();
        switch (place) {
            case START:
                builder
                        .append(Color)
                        .append(pattern)
                        .append(RESET_ANSI_SYMBOL)
                        .append(repeat(padding, SPACE));
                break;
            case MIDDLE:
                builder
                        .append(repeat(padding, SPACE))
                        .append(Color)
                        .append(pattern)
                        .append(RESET_ANSI_SYMBOL)
                        .append(repeat(padding, SPACE));
                break;
            case END:
                builder
                        .append(repeat(padding, SPACE))
                        .append(Color)
                        .append(pattern)
                        .append(RESET_ANSI_SYMBOL);
        }
        return builder.toString();
    }
}
