package home.crskdev.tododagger.core.view.table.schema;

/**
 * Created by criskey on 5/4/2017.
 */
public class NoSchema extends Schema<Void> {

    public NoSchema(int width) {
        super(width, Void.class);
    }

}
