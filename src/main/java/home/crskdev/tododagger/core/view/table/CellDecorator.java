package home.crskdev.tododagger.core.view.table;

import home.crskdev.tododagger.core.view.Decorator;

/**
 * Created by criskey on 6/4/2017.
 */
public interface CellDecorator {

    /**<b>Must return null</b> if you don't want to decorate a particular cell given by row and col
     * @param row
     * @param col
     * @return a decorated cell
     */
    String decorate(int row, int col, Decorator.Builder decorator, Cell cell);

}
