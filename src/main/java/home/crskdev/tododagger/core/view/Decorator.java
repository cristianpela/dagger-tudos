package home.crskdev.tododagger.core.view;

import static home.crskdev.tododagger.core.view.ANSI.Color.RESET;
import static home.crskdev.tododagger.core.view.ANSI.Color.toBackground;

/**
 * Created by criskey on 6/4/2017.
 */
public final class Decorator {

    private final String content;

    private StringBuilder ansiEscapes;

    Decorator(String content, ANSI.Color foreground, ANSI.Color background,
              boolean bold, boolean italic, boolean underlined, boolean blink) {
        this.content = content;

        ansiEscapes = new StringBuilder(RESET.toString());
        if (foreground != null) {
            ansiEscapes.append(foreground.toString());
        }
        if (background != null) {
            ansiEscapes.append(toBackground(background));
        }
        if (bold) {
            ansiEscapes.append(ANSI.Format.BOLD);
        }
        if (italic) {
            //TODO italic
            //ansiEscapes.append("??")
        }
        if (underlined) {
            ansiEscapes.append(ANSI.Format.UNDERLINED);
        }
        if (blink) {
            ansiEscapes.append(ANSI.Format.BLINK);
        }

    }

    private String get() {
        return ansiEscapes
                .append(content)
                .append(RESET.toString())
                .toString();
    }

    public static Builder builder(String content) {
        return new Builder(content);
    }

    public static class Builder {

        private String content;
        private ANSI.Color foreground;
        private ANSI.Color background;
        private boolean bold;
        private boolean italic;
        private boolean underlined;
        private boolean blink;

        private Builder(String content) {
            this.content = content;
        }

        public Builder foreground(ANSI.Color foreground) {
            this.foreground = foreground;
            return this;
        }

        public Builder background(ANSI.Color background) {
            this.background = background;
            return this;
        }

        public Builder bold() {
            this.bold = true;
            return this;
        }

        public Builder italic() {
            this.italic = true;
            return this;
        }

        public Builder underlined() {
            this.underlined = true;
            return this;
        }

        public Builder blink() {
            this.blink = true;
            return this;
        }

        public String create() {
            return new Decorator(content,
                    foreground,
                    background,
                    bold,
                    italic,
                    underlined,
                    blink)
                    .get();
        }
    }
}
