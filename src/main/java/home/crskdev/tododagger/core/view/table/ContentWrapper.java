package home.crskdev.tododagger.core.view.table;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by criskey on 5/4/2017.
 */
public class ContentWrapper {

    public static final ContentWrapper INSTANCE = new ContentWrapper();

    private ContentWrapper() {}

    /**
     * Return a list of content chunks, each chunk has a length of width (last one may have less)
     *
     * @param width
     * @param content
     * @return
     */
    public List<String> wrap(int width, String content) {
        List<String> bucket = new ArrayList<>();
        findChuncks(bucket, width, content);
        return bucket;
    }

    /**
     * Recursevly find chucks for content according to colWidth
     *
     * @param bucket
     * @param colWidth
     * @param currColContent
     */
    private void findChuncks(List<String> bucket, int colWidth, String currColContent) {
        if (currColContent.length() <= colWidth) {
            bucket.add(currColContent);
            return;
        }
        bucket.add(currColContent.substring(0, colWidth));
        findChuncks(bucket, colWidth, currColContent.substring(colWidth));
    }
}
