package home.crskdev.tododagger.core.view.table;

import home.crskdev.tododagger.core.view.ANSI.Color;

/**
 * Created by criskey on 31/3/2017.
 */
public final class DividersSpec {

    public static final DividersSpec NO_DIVIDER = new DividersSpec(' ');

    public final char vertical;
    public final char horizontal;

    public final Color vColor;
    public final Color hColor;

    public DividersSpec(char horizontal, Color hColor, char vertical, Color vColor) {
        this.hColor = hColor;
        this.vertical = vertical;
        this.horizontal = horizontal;
        this.vColor = vColor;
    }

    public DividersSpec(char any, Color vColor, Color hColor) {
        this(any, hColor, any, vColor);
    }

    public DividersSpec(Color vColor, Color hColor) {
        this('-', vColor, '|', hColor);
    }

    public DividersSpec(Color Color) {
        this('-', Color, '|', Color);
    }

    public DividersSpec(char horizontal, char vertical) {
        this(horizontal, Divider.RESET_ANSI_SYMBOL, vertical, Divider.RESET_ANSI_SYMBOL);
    }

    public DividersSpec(char any) {
        this(any, Divider.RESET_ANSI_SYMBOL, any, Divider.RESET_ANSI_SYMBOL);
    }

}
