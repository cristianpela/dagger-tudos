package home.crskdev.tododagger.core.view.table;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by criskey on 5/4/2017.
 */
public class RenderedCell {

    private final int width;

    private final String content;

    private final Alignment alignment;

    public RenderedCell(int width, String content) {
        this(width, Alignment.Type.LEFT, content);
    }

    public RenderedCell(int width, Alignment.Type alignType, String content) {
        this.width = width;
        this.content = content;
        this.alignment = new Alignment(alignType, width);
    }

    public List<String> getContent() {
        List<String> wrap = ContentWrapper.INSTANCE.wrap(width, content);
        return wrap.stream()
                .map((String w) -> alignment.align(w))
                .collect(Collectors.toList());
    }

}
