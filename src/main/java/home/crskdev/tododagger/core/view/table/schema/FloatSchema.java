package home.crskdev.tododagger.core.view.table.schema;

/**
 * Created by criskey on 5/4/2017.
 */
public class FloatSchema extends Schema<Float> {

    public final int decimals;

    public FloatSchema(int width, int decimals) {
        super(width, Float.class);
        this.decimals = decimals;
    }
}
