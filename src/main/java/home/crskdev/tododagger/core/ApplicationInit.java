package home.crskdev.tododagger.core;

import home.crskdev.tododagger.auth.di.AuthComponent;
import home.crskdev.tododagger.core.command.CommandReader;

import javax.inject.Inject;
import javax.inject.Provider;

/**
 * Created by criskey on 19/3/2017.
 */
public class ApplicationInit implements Init {
    @Inject
    public ApplicationInit(CommandReader reader, Provider<AuthComponent.Builder> builder) {
        //manager is used as parameter to notify dagger to create a instance of it
        builder.get().build().init();
        reader.start();
    }
}
