package home.crskdev.tododagger.core;

import dagger.Component;

import javax.inject.Named;
import javax.inject.Singleton;

/**
 * Created by criskey on 13/3/2017.
 */
@Component(modules = {ApplicationModule.class})
@Singleton
public interface ApplicationComponent extends InitComponent {

    @Override
    @Named("main-entry-point")
    Init init();
}
