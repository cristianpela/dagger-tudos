package home.crskdev.tododagger.core;

/**
 * Flag interface used to mark an entry point in component and "black-box" the entry
 * Created by criskey on 21/3/2017.
 */
public interface Init {}
