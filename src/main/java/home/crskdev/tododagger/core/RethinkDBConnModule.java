package home.crskdev.tododagger.core;

import com.rethinkdb.gen.ast.Db;
import com.rethinkdb.net.Connection;
import dagger.Module;
import dagger.Provides;

import javax.inject.Singleton;
import java.io.Closeable;

import static com.rethinkdb.RethinkDB.r;


/**
 * Created by criskey on 31/3/2017.
 */
@Module
public class RethinkDBConnModule {

    private String domain;

    private int port;

    public RethinkDBConnModule(String domain, int port) {
        this.domain = domain;
        this.port = port;
    }

    @Singleton
    @Provides
    public LazyCache<Connection> provideConnection() {
        return new LazyCache<Connection>() {
            @Override
            protected Connection init() {
                return r.connection().hostname(domain).port(port).connect();
            }
        };
    }

    @Singleton
    @Provides
    public Closeable provideClosable(LazyCache<Connection> connection) {
        return () -> connection.get().close();
    }

    @Provides
    public Db providesDb() {
        return r.db("test");
    }
}
