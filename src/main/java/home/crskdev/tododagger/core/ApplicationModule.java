package home.crskdev.tododagger.core;

import dagger.Module;
import dagger.Provides;
import home.crskdev.tododagger.account.details.UserDetails;
import home.crskdev.tododagger.auth.di.AuthComponent;
import home.crskdev.tododagger.auth.model.User;
import home.crskdev.tododagger.core.command.CommandReader;
import home.crskdev.tododagger.core.command.ListenerSwapper;
import home.crskdev.tododagger.core.command.ScannerCommandReader;
import home.crskdev.tododagger.core.view.ANSI;
import home.crskdev.tododagger.core.view.SystemView;
import home.crskdev.tododagger.core.view.View;
import home.crskdev.tododagger.todos.todo.model.Todo;

import javax.inject.Named;
import javax.inject.Provider;
import javax.inject.Singleton;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by criskey on 13/3/2017.
 */
@Module(subcomponents = AuthComponent.class, includes = RethinkDBConnModule.class)
public class ApplicationModule {

    private Map<String, List<Todo>> usersTodos;

    private Set<User> users;

    private Map<String, UserDetails> usersDetails;

    public ApplicationModule(Map<String, List<Todo>> usersTodos,
                             Set<User> users,
                             Map<String, UserDetails> usersDetails) {
        this.usersTodos = usersTodos;
        this.users = users;
        this.usersDetails = usersDetails;
    }

    @Singleton
    @Provides
    public Set<User> provideRegisteredUsers() {
        return users;
    }

    @Provides
    @Singleton
    public Map<String, List<Todo>> provideUsersTodos() {
        return usersTodos;
    }

    @Provides
    @Singleton
    public Map<String, UserDetails> provideUsersDetails() {
        return usersDetails;
    }

    @Provides
    @Singleton
    public CommandReader provideReader() {
        return new ScannerCommandReader();
    }

    @Provides
    @Singleton
    public ListenerSwapper provideListenerSetter(CommandReader reader) {
        return reader;
    }

    @Provides
    @Singleton
    public View provideSystemView(ANSI ansi) {
        return new SystemView(ansi);
    }

    @Provides
    @Named("main-entry-point")
    public Init providesEntryPoint(CommandReader reader, Provider<AuthComponent.Builder> builder) {
        return new ApplicationInit(reader, builder);
    }
}
