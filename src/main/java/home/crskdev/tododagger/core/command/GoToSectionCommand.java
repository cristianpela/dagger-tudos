package home.crskdev.tododagger.core.command;

import home.crskdev.tododagger.core.InitComponent;

import javax.inject.Provider;

/**
 * Created by criskey on 21/3/2017.
 */
public abstract class GoToSectionCommand<I extends  InitComponent.Builder<? extends InitComponent>> implements Command {

    private Provider<I> provider;

    public GoToSectionCommand(Provider<I> provider) {
        this.provider = provider;
    }

    @Override
    public void execute(String... args) throws CommandException {
        provider.get().build().init();
    }

    @Override
    public boolean isDefault() {
        return false;
    }
}
