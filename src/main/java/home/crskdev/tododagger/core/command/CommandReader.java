package home.crskdev.tododagger.core.command;

/**
 * Created by criskey on 14/3/2017.
 */
public interface CommandReader extends ListenerSwapper {

    String readLine();

    void start();

    void stop();

}
