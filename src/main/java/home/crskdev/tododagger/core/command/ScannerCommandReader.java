package home.crskdev.tododagger.core.command;

import javax.inject.Inject;
import java.util.Scanner;

/**
 * Created by criskey on 14/3/2017.
 */
public class ScannerCommandReader implements CommandReader, ListenerSwapper {

    private final Scanner scanner;

    private boolean running;

    private CommandListener listener;

    @Inject
    public ScannerCommandReader() {
        scanner = new Scanner(System.in);
        running = false;
    }

    @Override
    public void swapWith(CommandListener listener) {
        this.listener = listener;
    }

    @Override
    public String readLine() {
        return scanner.nextLine();
    }

    @Override
    public void start() {
        assert listener != null : "Reader has no CommandListener";
        running = true;
        while (running) {
            String readLine = scanner.nextLine();
            listener.onReadLine(readLine);
        }
    }

    @Override
    public void stop() {
        running = false;
    }

}
