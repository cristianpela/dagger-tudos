package home.crskdev.tododagger.core.command;

/**
 * Created by criskey on 19/3/2017.
 */
public interface ListenerSwapper {

    void swapWith(CommandListener listener);

}
