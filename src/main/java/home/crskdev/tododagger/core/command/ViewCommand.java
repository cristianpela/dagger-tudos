package home.crskdev.tododagger.core.command;

import home.crskdev.tododagger.core.view.View;

/**
 * Created by criskey on 24/3/2017.
 */
public abstract class ViewCommand implements Command {

    protected View view;

    public ViewCommand(View view) {
        this.view = view;
    }
}
