package home.crskdev.tododagger.core.command;


import home.crskdev.tododagger.core.view.ANSI;
import home.crskdev.tododagger.core.view.Decorator;
import home.crskdev.tododagger.core.view.View;

import java.io.Closeable;

/**
 * Created by criskey on 21/3/2017.
 */
public class SystemExitCommand extends ViewCommand {

    public static final String EXIT = "exit";

    private Closeable connection;

    public SystemExitCommand(View view, Closeable connection) {
        super(view);
        this.connection = connection;
    }

    @Override
    public void execute(String... args) throws CommandException {
        view.println(Decorator.builder("Good bye")
                .foreground(ANSI.Color.YELLOW)
                .create());
        try {
            connection.close();
            System.exit(0);
        } catch (Exception e) {
            view.println(Decorator
                    .builder("Error closing connection on exit. Reason: " + e.getMessage())
                    .foreground(ANSI.Color.RED).create());
            System.exit(1);
        }
    }

    @Override
    public String name() {
        return EXIT;
    }

    @Override
    public boolean isDefault() {
        return false;
    }
}
