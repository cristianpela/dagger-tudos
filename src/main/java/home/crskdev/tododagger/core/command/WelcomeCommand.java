package home.crskdev.tododagger.core.command;

import home.crskdev.tododagger.core.view.ANSI;
import home.crskdev.tododagger.core.view.Decorator;
import home.crskdev.tododagger.core.view.View;

import java.util.Arrays;

/**
 * Created by criskey on 21/3/2017.
 */
public abstract class WelcomeCommand extends ViewCommand {

    public static final String WELCOME = "welcome";

    private String principal;

    public WelcomeCommand(View view, String principal) {
        super(view);
        this.principal = principal;
    }

    @Override
    public void execute(String... args) throws CommandException {
        StringBuilder builder = new StringBuilder();
        builder.append("Hi ");
        builder.append(principal);
        builder.append(". The commands for this section are:");
        view.println(builder.toString());
        builder.setLength(0); // reset

        String[] cmds = availableCommands();
        Arrays.sort(cmds);
        for (int i = 0; i < cmds.length; i++) {
            builder.append(i == 0 ? "\t" : "");
            builder.append(cmds[i]);
            builder.append(i < cmds.length - 1 ? " | " : "");
        }
        view.println(Decorator.builder(builder.toString())
                .foreground(ANSI.Color.MAGENTA)
                .create());
    }

    public abstract String[] availableCommands();

    @Override
    public String name() {
        return WELCOME;
    }

    @Override
    public boolean isDefault() {
        return true;
    }
}
