package home.crskdev.tododagger.core.command;

import home.crskdev.tododagger.core.Init;
import home.crskdev.tododagger.core.view.ANSI;
import home.crskdev.tododagger.core.view.Decorator;
import home.crskdev.tododagger.core.view.View;

import java.util.Map;

/**
 * Created by criskey on 19/3/2017.
 */
public class CommandManager implements CommandListener, Init {

    protected final Map<String, Command> commands;

    protected View view;

    public CommandManager(View view, Map<String, Command> commands,
                          ListenerSwapper swapper) {
        this.view = view;
        this.commands = commands;
        swapper.swapWith(this);
        for (Command cmd : commands.values()) {
            if (cmd.isDefault()) {
                cmd.execute(cmd.name());
                break;
            }
        }
    }

    @Override
    public void onReadLine(String readLine) {
        String[] args = readLine.trim().split(" ");
        String cmdName = args[0].trim().toLowerCase();
        Command command = commands.get(cmdName);
        if (command != null) {
            //don't allow default commands to be accessed from console
            if (!command.isDefault()) {
                try {
                    command.execute(args);
                } catch (CommandException ex) {
                    error(ex.getMessage());
                }
            } else {
                error("This command can't be run for console");
            }
        } else {
            error("Unknown command: " + readLine);
        }
    }

    private void error(String message) {
        view.println(Decorator.builder(message).foreground(ANSI.Color.RED).create());
    }
}
