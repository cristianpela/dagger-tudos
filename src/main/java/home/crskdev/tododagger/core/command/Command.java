package home.crskdev.tododagger.core.command;

/**
 * Created by criskey on 13/3/2017.
 */
public interface Command {

    void execute(String... args) throws CommandException;

    String name();

    boolean isDefault();

}
