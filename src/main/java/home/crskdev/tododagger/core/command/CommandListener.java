package home.crskdev.tododagger.core.command;

/**
 * Created by criskey on 16/3/2017.
 */
public interface CommandListener {

    void onReadLine(String readLine);
}
