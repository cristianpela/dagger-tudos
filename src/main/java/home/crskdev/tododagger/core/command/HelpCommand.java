package home.crskdev.tododagger.core.command;

import home.crskdev.tododagger.core.view.View;
import home.crskdev.tododagger.core.view.table.DividersSpec;
import home.crskdev.tododagger.core.view.table.Table;

import java.io.*;
import java.util.Enumeration;
import java.util.Properties;

/**
 * Created by criskey on 13/3/2017.
 */
public abstract class HelpCommand implements Command {

    public static final String HELP = "help";

    private View view;

    public HelpCommand(View view) {
        this.view = view;
    }

    @Override
    public void execute(String... cmd) throws CommandException {
        try (InputStream in = load()) {
            Table table = new Table(view, DividersSpec.NO_DIVIDER, 10, 50);
            Properties prop = new Properties();
            prop.load(in);
            Enumeration<?> e = prop.propertyNames();
            while (e.hasMoreElements()) {
                String key = (String) e.nextElement();
                table.addRow(key, prop.getProperty(key));
            }
            table.display();
        } catch (IOException ex) {
            throw new CommandException(ex.getMessage());
        }
    }

    private InputStream load() throws FileNotFoundException {
        String fileName = helpFileName();
        String ext = ".properties";
        if(!fileName.endsWith(ext)){
            fileName += ext;
        }
        String pathname = "help/" + fileName;
        File file = new File(pathname);
        if (!file.exists())
            return getClass().getClassLoader().getResourceAsStream(pathname);
        return new FileInputStream(file);
    }

    @Override
    public String name() {
        return HELP;
    }

    @Override
    public boolean isDefault() {
        return false;
    }

    /**
     * File must be a property file, in the help directory
     *
     * @return file name
     */
    public abstract String helpFileName();

}
