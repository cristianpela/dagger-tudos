package home.crskdev.tododagger.core;

/**
 * Created by criskey on 1/4/2017.
 */
public abstract class LazyCache<T>  {

    protected T cache;

    public T get() {
        return (cache == null) ? cache = init() : cache;
    }

    protected abstract T init();

}
