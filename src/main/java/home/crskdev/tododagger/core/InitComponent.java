package home.crskdev.tododagger.core;

/**
 * Created by criskey on 21/3/2017.
 */
public interface InitComponent {

    Init init();

    interface Builder <I extends InitComponent>{
        I build();
    }
}
