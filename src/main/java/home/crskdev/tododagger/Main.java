package home.crskdev.tododagger;

import home.crskdev.tododagger.account.details.UserDetails;
import home.crskdev.tododagger.auth.model.User;
import home.crskdev.tododagger.core.ApplicationModule;
import home.crskdev.tododagger.core.DaggerApplicationComponent;
import home.crskdev.tododagger.core.RethinkDBConnModule;
import home.crskdev.tododagger.todos.todo.model.Todo;

import java.util.*;

/**
 * Created by criskey on 8/3/2017.
 */
public class Main {

    public static void main(String[] args) {
        List<Todo> fooTodos = new ArrayList<>();
        fooTodos.add(new Todo("Do Stuff"));
        fooTodos.add(new Todo("Do More Stuff"));
        fooTodos.add(new Todo("Lorem Ipsum is simply dummy content of the printing and typesetting industry. " +
                "Lorem Ipsum has been the industry's standard dummy content ever since the 1500s," +
                " when an unknown printer took a galley of type and scrambled it to make a type specimen book." +
                " It has survived not only five centuries, but also the leap into electronic typesetting,"));

        Map<String, List<Todo>> usersTodos = new HashMap<>();
        usersTodos.put("foo", fooTodos);

        List<Todo> barTodos = new ArrayList<>();
        barTodos.add(new Todo("Bar Stuff"));
        barTodos.add(new Todo("Do More Bar Stuff"));
        usersTodos.put("bar", barTodos);

        Set<User> users = new HashSet<>();
        users.add(new User("foo", "123".toCharArray()));
        users.add(new User("bar", "123".toCharArray()));


        Map<String, UserDetails> usersDetails = new HashMap<>();
        usersDetails.put("foo", UserDetails.empty("foo"));
        usersDetails.put("bar", UserDetails.empty("bar"));

        DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(usersTodos, users, usersDetails))
                .rethinkDBConnModule(new RethinkDBConnModule("localhost", 28015))
                .build()
                .init();
        System.out.println("Working");

    }

}
