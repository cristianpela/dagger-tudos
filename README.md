A console like todo concept program, by using the command pattern, mostly. The command pattern is implemented using Dagger's 2
[Multibindings](https://google.github.io/dagger/multibindings.html) feature


```
#!java

@TodoScope
@Subcomponent(modules = TodosModule.class)
public interface TodosComponent extends InitComponent {

    @Override
    @Named("todos-entry-point")
    Init init();

    @Subcomponent.Builder
    interface Builder extends InitComponent.Builder<TodosComponent> {
        TodosComponent build();
    }
}
```

```
#!java

@Module
public class TodosModule {

    @Provides
    @Named("todos-entry-point")
    public Init provideEntryPoint(View view, @TodoCommands Map<String, Command> commands,
                                  ListenerSwapper setter) {
        return new CommandManager(view, commands, setter);
    }
    //Example of command mapping
    @Provides
    @IntoMap
    @TodoCommands
    @StringKey(TODOS)
    public Command provideListCommand(View view, @Proxy ITodoService service) {
        return new ListCommand(view, service);
    }
}
```




![dagger-todos-account.png](https://bitbucket.org/repo/XXrgBAA/images/2878299348-dagger-todos-account.png).

Flexible data fetching services. For now supporting in-memory-data and RethinkDB  

```
#!java

    @Provides
    @TodoScope
    @InMemory
    public ITodoService provideInMemoryTodoService(@Principal String principal, Map<String, List<Todo>> usersTodos) {
        return new InMemoryTodoService(principal, usersTodos);
    }

    @Provides
    @TodoScope
    @Rethink
    public ITodoService provideRethinkTodoService(@Principal String principal, LazyCache<Connection> lazyConnnection) {
        return new RethinkTodoService(principal, lazyConnnection);
    }

    @Provides
    @TodoScope
    @Proxy
    public ITodoService provideProxyTodoService(@Rethink ITodoService delegate) {
        return new TodoServiceProxy(delegate);
    }
```



Console generated tables, with custom schemas and cell rendering :

```
#!java

Table table = new Table(view,
                new StringSchema(5),
                new DateSchema(21),
                new StringSchema(35),
                new BooleanSchema(11));
        //we center the ID column
        table.addCellRenderer((row, col, cell) -> {
            if(col == 0 && row > 0){
                String id = cell.content.toString();
                return new RenderedCell(cell.schema.width,
                        Alignment.Type.CENTER,
                        id+").");
            }
            return null;
        });
```

![dagger-todos-table-2.png](https://bitbucket.org/repo/XXrgBAA/images/1763740504-dagger-todos-table-2.png)